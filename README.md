# Clean Insights SDK

[![Version](https://img.shields.io/cocoapods/v/CleanInsightsSDK.svg?style=flat)](https://cocoapods.org/pods/CleanInsightsSDK)
[![License](https://img.shields.io/cocoapods/l/CleanInsightsSDK.svg?style=flat)](https://cocoapods.org/pods/CleanInsightsSDK)
[![Platform](https://img.shields.io/cocoapods/p/CleanInsightsSDK.svg?style=flat)](https://cocoapods.org/pods/CleanInsightsSDK)

Clean Insights gives developers a way to plug into a secure, private measurement platform. 
It is focused on assisting in answering key questions about app usage patterns, and not on 
enabling invasive surveillance of all user habits. 
Our approach provides programmatic levers to pull to cater to specific use cases and privacy needs. 
It also provides methods for user interactions that are ultimately empowering instead of alienating.

## Example

```swift

    // Instantiate with configuration, best done in `AppDelegate`.
    let ci = try! CleanInsights(jsonConfigurationFile: 
            Bundle.main.url(forResource: "cleaninsights", withExtension: "json")!)

    // Ask for consent:
    ci.requestConsent(forCampaign: "test", ConsentRequestUi())

    // Measure a page visit, e.g. in `ViewController#viewDidAppear`:
    ci.measure(visit: ["Main"], forCampaign: "test")

    // Measure an event (e.g. a button press):
    ci.measure(event: "music", "play", forCampaign: "test")

    // Make sure to persist the locally cached data. E.g. on `AppDelegate#applicationDidEnterBackground`.
    ci.persist()
```

*Please note*: `CleanInsights`' core concept is a **Campaign**. Since we don't want you to
record just *anything*, like all the others do, you need to configure, what your actually interested
in and for how long. For a deeper understanding, please read the [Concepts](#concepts) section below.

This project also contains multiple example apps for the different platforms. 
Please refer to the [GitLab repository](https://gitlab.com/cleaninsights/clean-insights-apple-sdk)
for a full understanding on how to use this library.

### Reduced complexity "auto-tracker"

Since version 2.7.0, Clean Insights also contains a reduced "auto-tracker" version:

```swift

    let ci = CleanInsights.autoTrack(server: URL(string: "https://myhost.example.com/ci/cleaninsights.php")!, siteId: 1)
    
    let ui = ConsentRequestUi() // You're implementation of a consent request UI.
    
    ci.requestConsent(forCampaign: "visits", ui) { consent in
        if consent?.granted == true {
            // Yay! Next start will be recorded.
        }
        else {
            // Nay! User doesn't like to get tracked.
        }
    }
    
    
    // Later in your e.g. "SettingsViewController":
    
    CleanInsights.autoTrackCi?.measure(visit: ["settings"], forCampaign: "visits")
```

This will set up a `CleanInsights` instance with a never-ending campaign called "visits" 
which automatically tracks app-starts.

Best call this from your `AppDelegate`!

You still will need to provide consent. Check out the 
[iOS example](https://gitlab.com/cleaninsights/clean-insights-apple-sdk/-/blob/master/Example/CleanInsightsSDK/AppDelegate.swift)
on how to do that in the most basic way.


## Installation

CleanInsightsSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CleanInsightsSDK'
```

## Further Documentation:

- [Main project page](http://cleaninsights.org/)

- [All source code](https://gitlab.com/cleaninsights)

- [Design documents and JSON schema specifications](https://gitlab.com/cleaninsights/clean-insights-design)

- [API documentation](https://cleaninsights.gitlab.io/clean-insights-apple-sdk/)

- [A Guide to Clean Consent UX](https://okthanks.com/blog/2021/5/14/clean-consent-ux)


## Concepts

### Measurements

A measurement is a single call to the SDK to measure a visit or an event.

Measurements are always aggregated immediately and only stored in that aggregated
form, in order to avoid a too high resolution and therefore unnecessary invasion into the users
privacy.

All measurements are done for a specific campaign you need to configure.

Measurements done against an unconfigured campaign are ignored, as well as measurements
done against a campaign which ran out or where the maximum length of days of data gathering
is crossed.

This helps you avoid unwanted measurements with left-over measurement code of older 
campaigns.


### Campaigns

A campaign has a period during which it is active and a maximum length of days during which 
data is gathered for a specific user after they consented to the measurements.

The specific start date of a campaign helps you coordinate campaigns across platforms.
Measurements done before a campaign start are, of course, ignored.

The days during which measurements take place are defined by the aggregation period length
in days and the number of periods.

After a user consented, measurements will start right away for as long as the current measurement
period still goes on. 

If you want to even further increase your users' anonymity guarantees and ensure that the first
measured period is a full one, you can configure that with the `strengthen_anonymity` 
configuration option, which will enforce the beginning of measurements only at the next full 
aggregation period.

At the end of an aggregation period, the campaign data will be automatically sent to your insights 
server.

If you configure a higher `numberOfPeriods` than 1, the next aggregation period will begin
immediately after the end of the first one and measurement will continue.


#### Events

In Contrast to visit measurements, event measurements support the complete 
[Matomo Event API](https://matomo.org/docs/event-tracking/).

This means you can also record numeric values, like e.g. time something takes.

You can configure the aggregation method to use, when the same event is recorded multiple 
times: Event values can be summed up or an average can be calculated.

This can be configured per campaign.


#### Not a Matomo Campaign

Note the difference to Matomo: CleanInsights' concept of a campaign doesn't map to Matomo's
concept of campaigns, which is mostly about finding out if a marketing campaign (like an advertisement
or a landing page) sent any additional visitors to a website.


### Consent

You need to ask your users for consent to your measurements.

There are 2 types of consent:

- Campaign consents: Each measurement campaign needs to get consent by the user. You need
to explain to the user what you want to measure, how long you want to measure it and why.

The SDK contains UI to help you with that.

- Common feature consent: Some data is orthogonal to the visits or events you want to measure,
like locale or device type used. Since these features are only ever recorded when doing
measurements for a campaign, consent needs to be gathered only once per user per feature.

A user might want to actively withdraw consent. The SDK supports that. Please make sure that
the user can actually do that.

The SDK provides UI to help you with that.


## Configuration

To make sure your CleanInsights configuration is valid, you can use our 
[JSON scheme to validate against.](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schemas/configuration.schema.json)

Here's an [online validator](https://www.jsonschemavalidator.net)

A [complete documentation](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schema-docs/README.md)
is generated from that JSON scheme. 


## Supported Backends

CleanInsights SDK currently supports Matomo as a backend via 
[CIMP](https://gitlab.com/cleaninsights/clean-insights-matomo-proxy).


## Author

Benjamin Erhart, berhart@netzarchitekten.com for the [Guardian Project](https://guardianproject.info)


## License

CleanInsightsSDK is available under the MIT license. See the [LICENSE](LICENSE) file for more info.
