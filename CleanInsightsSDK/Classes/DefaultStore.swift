//
//  DefaultStore.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 19.01.21.
//  Copyright © 2021 Guardian Project. All rights reserved.
//

import Foundation

/**
 Default implementation of a store. Stores the data in plist format in a given folder.
 If no folder is given, defaults to the app's `Application Support` directory on MacOS or its `Documents` directory elsewhere.

 The `#send` implementation just uses Cocoa's `URLRequest`.
 */
open class DefaultStore: Store {

    private static let storageFilename = "cleaninsights.plist"

    private lazy var queue = DispatchQueue(label: "org.cleaninsights.queue", qos: .background)

    private var storageFile: URL?

    /**
     - parameter args: The location where to read and persist accumulated data.
        Either in the key "storageFile", which is expected to contain the fully qualified URL to a file.
        Or a "storageDir" URL, which is expected to point to a directory.
        OPTIONAL.
        Defaults to the app's `Application Support` directory on MacOS or its `Documents` directory elsewhere.
     - parameter debug: Optional function to output debug messages.
     */
    public override init(_ args: [String : Any] = [:], _ debug: ((_ message: String) -> Void)? = nil) {
        let fm = FileManager.default

        // Support lightweight subclasses or invocations which only want
        // to override the storageFile location.
        if let storageFile = args["storageFile"] as? URL {
            self.storageFile = storageFile
        }
        else {
            var dir = args["storageDir"] as? URL

            if dir == nil {
                let searchPath: FileManager.SearchPathDirectory

                #if os(macOS)
                    searchPath = .applicationSupportDirectory
                #else
                    searchPath = .documentDirectory
                #endif

                dir = fm.urls(for: searchPath, in: .userDomainMask).first
            }

            storageFile = dir?.appendingPathComponent(DefaultStore.storageFilename)
        }

        super.init(args, debug)
    }

    required public init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    public override func load(_ args: [String : Any]) -> Store? {
        let debug = (args["debug"] as? (String) -> Void)

        debug?("Try to load store from \"\(storageFile?.path ?? "nil")\".")

        if let storageFile = storageFile,
           FileManager.default.isReadableFile(atPath: storageFile.path)
        {
            do {
                let data = try Data(contentsOf: storageFile)

                return try PropertyListDecoder().decode(Store.self, from: data)
            }
            catch {
                debug?(error.localizedDescription)
            }
        }

        return nil
    }

    public override func persist(_ async: Bool, _ done: @escaping (Error?) -> Void) {
        guard let storageFile = storageFile else {
            done(URLError(
                        .fileDoesNotExist,
                        userInfo: [NSLocalizedDescriptionKey: "No storage file available. Could not persist!"]))

            return
        }

        let work = DispatchWorkItem {
            do {
                let data = try CleanInsights.plistEncoder.encode(self)

                try data.write(to: storageFile)

                done(nil)
            }
            catch {
                done(error)
            }
        }

        if async {
            queue.async(execute: work)
        }
        else {
            queue.sync(execute: work)
        }
    }

    public override func send(_ data: Data, _ server: URL, _ timeout: TimeInterval, _ done: @escaping (Error?) -> Void) {
        queue.async {
            var request = URLRequest(url: server,
                                     cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                     timeoutInterval: timeout)
            request.httpMethod = "POST"
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = data

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    return done(error)
                }

                if let response = response as? HTTPURLResponse,
                   response.statusCode != 200 && response.statusCode != 204
                {
                    let msg = "HTTP Error \(response.statusCode): \(String(data: data ?? Data(), encoding: .utf8) ?? ""))"

                    return done(URLError(.badServerResponse, userInfo: [NSLocalizedDescriptionKey: msg]))
                }

                return done(nil)
            }
            task.resume()
        }
    }
}
