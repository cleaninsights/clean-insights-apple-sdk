//
//  ConsentRequestUi.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 20.10.20.
//

import Foundation

/**
 Implement this to provide a UI which explains your campaigns.

 Questions you should answer to the user:

 - Why do you need these measurements?
 - How long will they get collected?
 - What might attackers learn from these numbers?
 - How long will you store them on your servers?
 - What are you going to do with these measurements?
 - How will the user benefit from these measurements?
 */
@objc
public protocol ConsentRequestUi {

    typealias CompleteFeature = ((_ granted: Bool) -> Void)

    typealias CompleteCampaign = ((_ granted: Bool, _ length: NSNumber?) -> Void)

    typealias CompletedFeature = ((_ consent: FeatureConsent?) -> Void)

    typealias CompletedCampaign = ((_ consent: CampaignConsent?) -> Void)

    /**
     Will be called if it is necessary to ask the user for consent to record a common feature while measuring a campaign.

     - parameter feature: The feature to record. (e.g. user agent, locale)
     - parameter complete: The callback which will store the consent or the denial of it.
     */
    @objc
    func show(feature: Feature, _ complete: @escaping CompleteFeature)

    /**
     Will be called if it is necessary to ask the user for consent to a measurement campaign.

     - parameter campaignId: The campaign identifier.
     - parameter campaign: The campaign configuration.
     - parameter complete: The callback which will store the consent or the denial of it.
     */
    @objc
    func show(campaignId: String, campaign: Campaign, _ complete: @escaping CompleteCampaign)
}
