//
//  Event.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 28.09.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

open class Event: DataPoint  {

    public enum CodingKeys: String, CodingKey {
        case category
        case action
        case name
        case value
    }


    /**
     The event category. Must not be empty. (eg. Videos, Music, Games...)
     */
    public let category: String

    /**
     The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     */
    public let action: String

    /**
     The event name. OPTIONAL.
     */
    public let name: String?

    /**
     The event value. OPTIONAL.
     */
    public var value: Double?


    /**
     - parameter category: The event category. Must not be empty. (eg. Videos, Music, Games...)
     - parameter action: The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     - parameter name: The event name. OPTIONAL.
     - parameter value: The event value. OPTIONAL.
     - parameter campaignId: The campaign ID this data point is for.
     - parameter times: Number of times this data point has arisen between `first` and `last`. OPTIONAL, defaults to 1.
     - parameter first: The first time this data point has arisen. OPTIONAL, defaults to now.
     - parameter last: The last time this data point has arisen. OPTIONAL, defaults to now.
     */
    public init(category: String, action: String, name: String? = nil, value: Double? = nil, campaignId: String, times: UInt = 1, first: Date = Date(), last: Date = Date()) {
        self.category = category
        self.action = action
        self.name = name
        self.value = value

        super.init(campaignId: campaignId, times: times, first: first, last: last)
    }


    // MARK: Codable

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        category = try container.decode(String.self, forKey: .category)
        action = try container.decode(String.self, forKey: .action)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        value = try container.decodeIfPresent(Double.self, forKey: .value)

        try super.init(from: decoder)
    }

    open override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(category, forKey: .category)
        try container.encode(action, forKey: .action)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(value, forKey: .value)

        try super.encode(to: encoder)
    }


    // MARK: Public Methods

    open override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? Event else {
            return false
        }

        return category == object.category
        && action == object.action
        && name == object.name
        && value == object.value
        && super.isEqual(object)
    }

    open override var description: String {
        return String(format: "[%@: category=%@, action=%@, name=%@, value=%@, campaignId=%@, times=%d, first=%@, last=%@]",
                      String(describing: type(of: self)), category, action, name ?? "nil",
                      value == nil ? "nil" : String(value!), campaignId, times,
                      first.description, last.description)
    }
}
