//
//  CleanInsights.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 25.09.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

@objc
open class CleanInsights: NSObject {

    /**
     The used configuration.
     */
    @objc
    public let conf: Configuration

    private var store: Store

    private var persistenceCounter = 0

    private var sending = false

    static let plistEncoder = PropertyListEncoder()

    private lazy var jsonEncoder: JSONEncoder = {
        let encoder = JSONEncoder()

        encoder.dateEncodingStrategy = .secondsSince1970

        if #available(iOS 13.0, macOS 10.15, watchOS 6.0, tvOS 13.0, *) {
            encoder.outputFormatting = .withoutEscapingSlashes
        }

        return encoder
    }()

    private var failedSubmissionCount = 0
    private var lastFailedSubmission = Date(timeIntervalSince1970: 0)

    /**
     - parameter configuration: The Configuration provided as a `Configuration` object.
     - parameter store: Your implementation of a `Store`.
     - parameter consentsFile: A file where only the consents are stored. Useful to import consents in extensions. See #storeConsents:to
     */
    @objc
    public init(configuration: Configuration, store: Store, consentsFile: URL? = nil) {
        conf = configuration

        self.store = store

        super.init()

        if !conf.check(debug: debug) {
            fatalError("Invalid configuration provided.")
        }

        if let consentsFile = consentsFile {
            loadConsents(from: consentsFile)
        }

        debug("User Agent=\(Insights.ua ?? "nil")")
    }


    /**
     - parameter configuration: The Configuration provided as a `Configuration` object.
     - parameter storageDir: The location where to read and persist accumulated data. OPTIONAL.
        Defaults to the app's `Application Support` directory on MacOS or its `Documents` directory elsewhere.
     - parameter consentsFile: A file where only the consents are stored. Useful to import consents in extensions. See #storeConsents:to
     */
    @objc
    public convenience init(configuration: Configuration, storageDir: URL? = nil, consentsFile: URL? = nil) {
        var args = [String: Any]()

        if let storageDir = storageDir {
            args["storageDir"] = storageDir
        }

        self.init(
            configuration: configuration,
            store: DefaultStore(args, {
                CleanInsights.debug(configuration.debug, $0)
            }),
            consentsFile: consentsFile)
    }

    /**
     - parameter jsonConfigurationFile: The Configuration provided as a URL to a JSON file
        which can be deserialized to a `Configuration` object.
     - parameter storageDir: The location where to read and persist accumulated data. OPTIONAL.
        Defaults to the app's `Documents` directory.
     - parameter consentsFile: A file where only the consents are stored. Useful to import consents in extensions. See #storeConsents:to
     */
    @objc
    public convenience init(jsonConfigurationFile: URL, storageDir: URL? = nil, consentsFile: URL? = nil) throws {
        self.init(configuration: try CleanInsights.decode(jsonConfigurationFile),
                  storageDir: storageDir,
                  consentsFile: consentsFile)
    }

    /**
     - parameter jsonConfigurationFile: The Configuration provided as a URL to a JSON file
        which can be deserialized to a `Configuration` object.
     - parameter store: Your implementation of a `Store`.
     - parameter consentsFile: A file where only the consents are stored. Useful to import consents in extensions. See #storeConsents:to
     */
    @objc
    public convenience init(jsonConfigurationFile: URL, store: Store, consentsFile: URL? = nil) throws {
        self.init(configuration: try CleanInsights.decode(jsonConfigurationFile),
                  store: store,
                  consentsFile: consentsFile)
    }


    // MARK: Measurements

    /**
     Track a scene visit.

     - parameter scenePath: A hierarchical path best describing the structure of your scenes. E.g. `['Main', 'Settings', 'Some Setting']`.
     - parameter campaignId: The campaign ID as per your configuration, where this measurement belongs to.
     */
    @objc
    public func measure(visit scenePath: [String], forCampaign campaignId: String) {
        if let campaign = getCampaignIfGood(campaignId, scenePath.joined(separator: "/"))
        {
            if let visit = getAndMeasure(store.visits, campaignId, campaign, { $0.scenePath == scenePath }) {
                debug("Gain visit insight: \(visit)")
            }
            else {
                // Align first and last timestamps with campaign measurement period,
                // in order not to accidentally leak more information than promised.
                if let period = campaign.currentMeasurementPeriod {
                    let visit = Visit(scenePath: scenePath, campaignId: campaignId,
                                      first: period.start, last: period.end)
                    store.visits.append(visit)

                    debug("Gain visit insight: \(visit)")
                }
                else {
                    debug("campaign.currentMeasurementPeriod == nil! This should not happen!")
                }
            }
        }

        persistAndSend()
    }

    /**
     Track an event.

     - parameter category: The event category. Must not be empty. (eg. Videos, Music, Games...)
     - parameter action: The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     - parameter campaignId: The campaign ID as per your configuration, where this measurement belongs to.
     - parameter name: The event name. OPTIONAL.
     */
    @objc
    @available(*, unavailable)
    public func measure(event category: String, _ action: String, forCampaign
                            campaignId: String, name: String? = nil)
    {
        // Workaround for Objective-C's problem of double not being an object, therefore cannot be nil.
        measure(event: category, action, forCampaign: campaignId, name: name, value: nil)
    }

    /**
     Track an event.

     - parameter category: The event category. Must not be empty. (eg. Videos, Music, Games...)
     - parameter action: The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     - parameter campaignId: The campaign ID as per your configuration, where this measurement belongs to.
     - parameter name: The event name. OPTIONAL.
     - parameter value: The event value. OPTIONAL.
     */
    @objc
    @available(*, unavailable)
    public func measure(event category: String, _ action: String, forCampaign
                            campaignId: String, name: String? = nil, value: Double)
    {
        // Workaround for Objective-C's problem of double not being an object, therefore cannot be nil.
        measure(event: category, action, forCampaign: campaignId, name: name, value: value)
    }

    /**
     Track an event.

     - parameter category: The event category. Must not be empty. (eg. Videos, Music, Games...)
     - parameter action: The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     - parameter campaignId: The campaign ID as per your configuration, where this measurement belongs to.
     - parameter name: The event name. OPTIONAL.
     - parameter value: The event value. OPTIONAL.
     */
    public func measure(event category: String, _ action: String, forCampaign
                            campaignId: String, name: String? = nil, value: Double? = nil)
    {
        if let campaign = getCampaignIfGood(campaignId, "\(category)/\(action)")
        {
            if let event = getAndMeasure(store.events, campaignId, campaign,
                                         { $0.category == category && $0.action == action && $0.name == name })
            {
                campaign.apply(value: value, to: event)

                debug("Gain event insight: \(event)")
            }
            else {
                // Align first and last timestamps with campaign measurement period,
                // in order not to accidentally leak more information than promised.
                if let period = campaign.currentMeasurementPeriod {
                    let event = Event(category: category, action: action, name: name, value: value,
                                      campaignId: campaignId, first: period.start, last: period.end)

                    store.events.append(event)

                    debug("Gain event insight: \(event)")
                }
                else {
                    debug("campaign.currentMeasurementPeriod == nil! This should not happen!")
                }
            }
        }

        persistAndSend()
    }


    // MARK: Consents

    @objc
    public var featureConsentsCount: Int {
        return store.consents.features.count
    }

    @objc
    public var campaignConsentsCount: Int {
        return store.consents.campaigns.count
    }

    @objc
    public func getFeatureConsent(byIndex index: Int) -> FeatureConsent? {
        let features = store.consents.features

        guard index < features.keys.count else {
            return nil
        }

        let i = features.index(features.startIndex, offsetBy: index)
        let feature = features.keys[i]

        return store.consents.consent(forFeature: feature)
    }

    @objc
    public func getCampaignConsent(byIndex index: Int) -> CampaignConsent? {
        let campaigns = store.consents.campaigns

        guard index < campaigns.keys.count else {
            return nil
        }

        let i = campaigns.index(campaigns.startIndex, offsetBy: index)
        let campaignId = campaigns.keys[i]

        return store.consents.consent(forCampaign: campaignId)
    }

    @objc
    @discardableResult
    public func grant(feature: Feature) -> FeatureConsent {
        let consent = store.consents.grant(feature: feature)

        persistAndSend()

        return consent
    }

    @objc
    @discardableResult
    public func deny(feature: Feature) -> FeatureConsent {
        let consent = store.consents.deny(feature: feature)

        persistAndSend()

        return consent
    }

    /**
     Returns the consent for a given feature, if any available.

     - parameter feature: The feature to get the consent for.
     - returns: the `FeatureConsent` for the given feature or `nil`, if consent unknown.
     */
    @objc
    public func consent(forFeature feature: Feature) -> FeatureConsent? {
        if conf.serverSideAnonymousUsage {
            return FeatureConsent(feature, Consent(granted: false))
        }

        return store.consents.consent(forFeature: feature)
    }

    /**
     Checks the consent state for a feature.

     - parameter feature: The feature to check the consent state for.
     - returns: the current state of consent.
     */
    @objc
    public func state(ofFeature feature: Feature) -> Consent.State {
        if conf.serverSideAnonymousUsage {
            return .denied
        }

        return store.consents.state(ofFeature: feature)
    }

    /**
     User consents to run a specific campaign.

     - parameter campaignId: The campaign ID.
     - parameter length: OPTIONAL. The custimized length of the measurement period, no longer than the maximum period.
     - returns: The created `CampaignConsent` or `nil` if a campaign with that ID is not configured.
     */
    @discardableResult
    public func grant(campaign campaignId: String, length: TimeInterval? = nil) -> CampaignConsent? {
        guard let campaign = conf.campaigns[campaignId] else {
            return nil
        }

        let consent = store.consents.grant(campaign: campaignId, campaign, length: length)

        persistAndSend()

        return consent
    }

    /**
     User consents to run a specific campaign.

     - parameter campaignId: The campaign ID.
     - parameter length: OPTIONAL. The custimized length of the measurement period, no longer than the maximum period.
     - returns: The created `CampaignConsent` or `nil` if a campaign with that ID is not configured.
     */
    @objc
    @available(*, unavailable)
    @discardableResult
    public func grant(campaign campaignId: String, length: NSNumber? = nil) -> CampaignConsent? {
        return grant(campaign: campaignId, length: length?.doubleValue)
    }

    @objc
    @discardableResult
    public func deny(campaign campaignId: String) -> CampaignConsent? {
        guard conf.campaigns[campaignId] != nil else {
            return nil
        }

        let consent = store.consents.deny(campaign: campaignId)

        persistAndSend()

        return consent
    }

    /**
     Checks the consent state for a campaign.

     - parameter campaignId: The campaign ID to check the consent for.
     - returns: `true`, if consent to run a campaign was given and is now valid.
     */
    @objc
    public func isCampaignCurrentlyGranted(_ campaignId: String) -> Bool {
        return state(ofCampaign: campaignId) == .granted
    }

    /**
     Returns the consent for a given campaign, if any available.

     - parameter campaignId: The campaign ID to get the consent for.
     - returns: the `CampaignConsent` for the given campaign or `nil`, if consent unknown.
     */
    @objc
    public func consent(forCampaign campaignId: String) -> CampaignConsent? {
        if conf.serverSideAnonymousUsage {
            return CampaignConsent(campaignId, Consent(granted: true))
        }

        guard let campaign = conf.campaigns[campaignId] else {
            return nil
        }

        guard Date() < campaign.end else {
            return nil
        }

        return store.consents.consent(forCampaign: campaignId)
    }

    /**
     Checks the consent state for a campaign.

     - parameter campaignId: The campaign ID to check the consent state for.
     - returns: the current state of consent.
     */
    @objc
    public func state(ofCampaign campaignId: String) -> Consent.State {
        if conf.serverSideAnonymousUsage {
            return .granted
        }

        guard let campaign = conf.campaigns[campaignId] else {
            return .unconfigured
        }

        guard Date() < campaign.end else {
            return .unconfigured
        }

        return store.consents.state(ofCampaign: campaignId)
    }

    @objc
    public func requestConsent(
        forCampaign campaignId: String,
        _ consentRequestUi: ConsentRequestUi,
        _ completed: ConsentRequestUi.CompletedCampaign? = nil)
    {
        guard let campaign = conf.campaigns[campaignId] else {
            debug("Cannot request consent: Campaign '\(campaignId)' not configured.")
            completed?(nil)
            return
        }

        guard Date() < campaign.end else {
            debug("Cannot request consent: End of campaign '\(campaignId)' reached.")
            completed?(nil)
            return
        }

        guard campaign.nextTotalMeasurementPeriod != nil else {
            debug("Cannot request consent: Campaign '\(campaignId)' configuration seems messed up.")
            completed?(nil)
            return
        }

        if let consent = store.consents.consent(forCampaign: campaignId) {
            debug("Already asked for consent for campaign '\(campaignId)'. It was \(consent.granted ? "granted between \(consent.start) and \(consent.end)" : "denied on \(consent.start)").")
            completed?(consent)
            return
        }

        let complete = { (granted: Bool, length: NSNumber?) in
            let consent: CampaignConsent

            if granted {
                consent = self.store.consents.grant(campaign: campaignId, campaign, length: length?.doubleValue)
            }
            else {
                consent = self.store.consents.deny(campaign: campaignId)
            }

            completed?(consent)
        }

        DispatchQueue.main.async {
            consentRequestUi.show(campaignId: campaignId, campaign: campaign, complete)
        }
    }

    @objc
    public func requestConsent(
        forFeature feature: Feature,
        _ consentRequestUi: ConsentRequestUi,
        _ completed: ConsentRequestUi.CompletedFeature? = nil)
    {
        if let consent = store.consents.consent(forFeature: feature) {
            debug("Already asked for consent for feature '\(feature)'. It was \(consent.granted ? "granted" : "denied") on \(consent.start).")
            completed?(consent)
            return
        }

        let complete = { (granted: Bool) in
            let consent: FeatureConsent

            if granted {
                consent = self.store.consents.grant(feature: feature)
            }
            else {
                consent = self.store.consents.deny(feature: feature)
            }

            completed?(consent)
        }

        DispatchQueue.main.async {
            consentRequestUi.show(feature: feature, complete)
        }
    }

    /**
     Store consents  to a file, so you can share it with an app extension or a watchOS app.

     - parameter file: The location to store to. Possibly in an app group container, so extensions can read it.
     */
    @objc
    public func storeConsents(to file: URL) {
        do {
            let data = try CleanInsights.plistEncoder.encode(store.consents)

            try data.write(to: file)

            debug("Consents stored to file=\(file)")
        }
        catch {
            debug(e: error)
        }
    }

    @objc
    public func loadConsents(from file: URL) {
        debug("Load consents from file=\(file)")

        if FileManager.default.isReadableFile(atPath: file.path)
        {
            do {
                let data = try Data(contentsOf: file)

                let consents = try PropertyListDecoder().decode(Consents.self, from: data)

                debug("Use consents loaded from shared consents file.")

                store.consents = consents
            }
            catch {
                debug(e: error)
            }
        }
        else {
            debug("Consents file doesn't exist or isn't readable.")
        }
    }


    // MARK: Helpers

    /**
     Sends an empty body to the server for easy debugging of server-related issues like TLS and CORS problems.

     **DON'T LEAVE THIS IN PRODUCTION**, once you're done fixing any server issues. There's absolutely no point in
     pinging the server with this all the time and it will undermine your privacy promise to your users!

     - parameter done: Callback, when the operation is finished, either successfully or not. OPTIONAL
     */
    public func testServer(_ done: ((Error?) -> Void)? = nil) {
        store.send(Data(), conf.server, conf.timeout) { error in
            let error = error ?? URLError(
                .unknown, userInfo: [
                    NSLocalizedDescriptionKey:
                        "Server replied with no error while it should have responded with HTTP 400 Bad Request!"])

            if error is URLError && error.localizedDescription.starts(with: "HTTP Error 400:") {
                self.debug("Successfully tested server.")

                done?(nil)

                return
            }

            self.debug(e: error)

            done?(error)
        }
    }


    // MARK: Persistence

    /**
     Persist accumulated data to the filesystem.

     The app should call this on `applicationDidEnterBackground:`.
     */
    @objc
    public func persist() {
        persist(async: false, force: true)
    }


    // MARK: Private Methods

    /**
     Persist accumulated data to the filesystem.

     - parameter async: If true, returns immediately and does persistence asynchronously, only if it's already due.
     - parameter force: Write regardless of threshold reached.
     */
    private func persist(async: Bool, force: Bool = false) {
        persistenceCounter += 1

        if force || persistenceCounter >= conf.persistEveryNTimes {
            store.persist(async) { error in
                if let error = error {
                    self.debug(e: error)
                }
                else {
                    self.persistenceCounter = 0

                    self.debug("Data persisted to storage.")
                }
            }
        }
    }

    /**
     Persist data asynchronously and send all data to the CleanInsights Matomo Proxy server.

     If sending was successful, remove sent data from store and persist again.
     */
    private func persistAndSend() {
        persist(async: true)

        guard !sending else {
            debug("Data sending already in progress.")

            return
        }

        sending = true

        if failedSubmissionCount > 0 {
            // Calculate a delay for the next retry:
            // Minimum is 2 times the configured network timeout after the first failure,
            // exponentially increasing with number of retries.
            // Maximum is every conf.maxRetryDelay interval.
            let exp = lastFailedSubmission.addingTimeInterval(conf.timeout * pow(2, Double(failedSubmissionCount)))
            let tru = lastFailedSubmission.addingTimeInterval(conf.maxRetryDelay)

            if Date() < min(exp, tru) {
                self.sending = false

                debug("Waiting longer to send data after \(failedSubmissionCount) failed attempts.")

                return
            }
        }

        let insights = Insights(conf, store)

        if insights.isEmpty {
            self.sending = false

            debug("No data to send.")

            return
        }

        let done = { (error: Error?) in
            if let error = error {
                self.lastFailedSubmission = Date()
                self.failedSubmissionCount += 1

                self.debug(e: error)
            }
            else {
                self.lastFailedSubmission = Date(timeIntervalSince1970: 0)
                self.failedSubmissionCount = 0

                self.debug("Successfully sent data.")

                insights.clean(store: self.store)

                self.persist(async: true, force: true)
            }

            self.sending = false
        }

        let body: Data

        do {
            body = try jsonEncoder.encode(insights)
        }
        catch {
            return done(error)
        }

        store.send(body, conf.server, conf.timeout, done)
    }


    private func getCampaignIfGood(_ campaignId: String, _ debugString: String) -> Campaign? {
        guard let campaign = conf.campaigns[campaignId] else {
            debug("Measurement '\(debugString)' discarded, because campaign '\(campaignId)' is missing in configuration.")
            return nil
        }

        let now = Date()

        guard now >= campaign.start else {
            debug("Measurement '\(debugString)' discarded, because campaign '\(campaignId)' didn't start, yet.")
            return nil
        }

        guard now <= campaign.end else {
            debug("Measurement '\(debugString)' discarded, because campaign '\(campaignId)' already ended.")
            return nil
        }

        guard isCampaignCurrentlyGranted(campaignId) else {
            debug("Measurement '\(debugString)' discarded, because campaign '\(campaignId)' has no user consent yet, any more or we're outside the measurement period.")
            return nil
        }

        return campaign
    }

    /**
     Get a `DataPoint` subclass out of the `haystack`, as long as it fits the `campaign`.
     Increases `times` according to the campaigns rules.

     Create a new `DataPoint` if nothing is returned here.

     - parameter haystack: The haystack full of `DataPoint` subclasses.
     - parameter campaignId: The campaign ID it must match.
     - parameter campaign: The campaign parameters to match against.
     - parameter where: Additional condition for selection.
     - returns: a `DataPoint` subclass out of the `haystack`, as long as it fits the `campaign`.
     */
    private func getAndMeasure<T: DataPoint>(_ haystack: [T], _ campaignId: String, _ campaign: Campaign, _ where: @escaping ((T) -> Bool)) -> T? {

        guard let period = campaign.currentMeasurementPeriod else {
            debug("campaign.currentMeasurementPeriod == nil! This should not happen!")
            return nil
        }

        if let dataPoint = haystack.first(where: {
            $0.campaignId == campaignId
                && $0.first >= period.start
                && $0.first <= period.end
                && $0.last >= period.start
                && $0.last <= period.end
                && `where`($0)
        }) {
            if !campaign.onlyRecordOnce {
                dataPoint.times += 1
            }

            return dataPoint
        }

        return nil
    }


    func debug(_ message: String) {
        CleanInsights.debug(conf.debug, message)
    }

    func debug(e: Error) {
        debug(e.localizedDescription)
    }

    private class func debug(_ toggle: Bool, _ message: String) {
        guard toggle else {
            return
        }

        print("[CleanInsightsSDK] \(message)")
    }

    private class func decode(_ jsonConfigurationFile: URL) throws -> Configuration {
        let data = try Data(contentsOf: jsonConfigurationFile)

        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(df)

        return try decoder.decode(Configuration.self, from: data)
    }
}
