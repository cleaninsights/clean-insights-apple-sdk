//
//  Visit.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 30.09.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

open class Visit: DataPoint {

    public enum CodingKeys: String, CodingKey {
        case scenePath = "action_name"
    }


    /**
     A hierarchical path to the scene visited.
     */
    public let scenePath: [String]


    /**
     - parameter scenePath: A hierarchical path to the scene visited.
     - parameter campaignId: The campaign ID this data point is for.
     - parameter times: Number of times this data point has arisen between `first` and `last`. OPTIONAL, defaults to 1.
     - parameter first: The first time this data point has arisen. OPTIONAL, defaults to now.
     - parameter last: The last time this data point has arisen. OPTIONAL, defaults to now.
     */
    public init(scenePath: [String], campaignId: String, times: UInt = 1, first: Date = Date(), last: Date = Date()) {
        self.scenePath = scenePath

        super.init(campaignId: campaignId, times: times, first: first, last: last)
    }


    // MARK: Codable

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        scenePath = try container.decode(String.self, forKey: .scenePath).components(separatedBy: "/")

        try super.init(from: decoder)
    }

    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(scenePath.joined(separator: "/"), forKey: .scenePath)

        try super.encode(to: encoder)
    }


    // MARK: Public Methods

    open override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? Visit else {
            return false
        }

        return scenePath == object.scenePath
        && super.isEqual(object)
    }

    open override var description: String {
        return String(format: "[%@: scenePath=%@, campaignId=%@, times=%d, first=%@, last=%@]",
                      String(describing: type(of: self)), scenePath, campaignId, times, first.description,
                      last.description)
    }
}
