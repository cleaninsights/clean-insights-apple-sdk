//
//  Locale+HttpAcceptLanguage.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 05.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//
import Foundation

extension Locale {

    static var httpAcceptLanguage: String {
        var components = [String]()

        for (index, languageCode) in preferredLanguages.enumerated() {
            let quality = 1.0 - (Double(index) * 0.1)

            components.append("\(languageCode);q=\(quality)")

            if quality <= 0.5 {
                break
            }
        }

        return components.joined(separator: ",")
    }
}
