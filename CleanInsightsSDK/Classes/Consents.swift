//
//  Consents.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 08.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

@objc
public enum Feature: Int, RawRepresentable, Codable {

    case lang
    case ua

    public init?(rawValue: String) {
        switch rawValue {
        case "ua":
            self = .ua

        default:
            self = .lang
        }
    }
}

@objc
open class Consent: NSObject, Codable {

    @objc
    public enum State: Int, RawRepresentable, Codable {

        /**
         A campaign with that ID doesn't exist or already expired.
         */
        case unconfigured

        /**
         There's no record of consent. User was probably never asked.
         */
        case unknown

        /**
         User denied consent. Don't ask again!
         */
        case denied

        /**
         Consent was given, but consent period has not yet started.
         */
        case notStarted

        /**
         Consent was given, but consent period is over. You might ask again for a new period.
         */
        case expired

        /**
         Consent was given and is currently valid.
         */
        case granted
    }


    @objc
    public let granted: Bool

    @objc
    public let start: Date

    @objc
    public let end: Date

    @objc
    public var state: State {
        return granted ? .granted : .denied
    }

    @objc
    public init(granted: Bool, _ start: Date = Date(), _ end: Date = Date()) {
        self.granted = granted
        self.start = start
        self.end = end

        super.init()
    }

    // MARK: Public Methods

    @objc
    open override var description: String {
        return String(format: "[%@: granted=%d, start=%@, end=%@]",
                      String(describing: type(of: self)), granted, start.description, end.description)
    }
}

@objc
open class FeatureConsent: Consent {

    public enum CodingKeys: String, CodingKey {
        case feature
    }

    @objc
    public let feature: Feature

    @objc
    public init(_ feature: Feature, _ consent: Consent) {
        self.feature = feature

        super.init(granted: consent.granted, consent.start, consent.end)
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        feature = try container.decode(Feature.self, forKey: .feature)

        try super.init(from: decoder)
    }

    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(feature, forKey: .feature)

        try super.encode(to: encoder)
    }
}

@objc
open class CampaignConsent: Consent {

    public enum CodingKeys: String, CodingKey {
        case campaignId
    }

    @objc
    public let campaignId: String

    @objc
    public override var state: Consent.State {
        guard granted else {
            return .denied
        }

        let now = Date()

        guard now >= start else {
            return .notStarted
        }

        guard now <= end else {
            return .expired
        }

        return .granted
    }

    @objc
    public init(_ campaignId: String, _ consent: Consent) {
        self.campaignId = campaignId

        super.init(granted: consent.granted, consent.start, consent.end)
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        campaignId = try container.decode(String.self, forKey: .campaignId)

        try super.init(from: decoder)
    }

    public override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(campaignId, forKey: .campaignId)

        try super.encode(to: encoder)
    }
}

/**
 This class keeps track of all granted or denied consents of a user.

 There are two different types of consents:
 - Consents for common features like if we're allowed to evaluate the locale or a user agent.
 - Consents per measurement campaign.

 The time of the consent is recorded along with it's state: If it was actually granted or denied.

 Consents for common features are given indefinitely, since they are only ever recorded along with running
 campaigns.

 Consents for campaigns only last for a certain amount of days.
 */
@objc
open class Consents: NSObject, Codable {

    var features = [Feature: Consent]()

    var campaigns = [String: Consent]()


    // MARK: Common Features
    /**
     User consents to evaluate a `Feature`.
     */
    @objc
    @discardableResult
    public func grant(feature: Feature) -> FeatureConsent {
        // Don't overwrite original grant timestamp.
        if features[feature] == nil || !features[feature]!.granted {
            features[feature] = Consent(granted: true)
        }

        return consent(forFeature: feature)!
    }

    /**
     User denies consent to evaluate a `Feature`.
     */
    @objc
    @discardableResult
    public func deny(feature: Feature) -> FeatureConsent {
        // Don't overwrite original deny timestamp.
        if features[feature] == nil || features[feature]!.granted {
            features[feature] = Consent(granted: false)
        }

        return consent(forFeature: feature)!
    }

    /**
     Returns the consent for a given feature, if any available.

     - parameter feature: The feature to get the consent for.
     - returns: the `FeatureConsent` for the given feature or `nil`, if consent unknown.
     */
    @objc
    public func consent(forFeature feature: Feature) -> FeatureConsent? {
        guard let consent = features[feature] else {
            return nil
        }

        return FeatureConsent(feature, consent)
    }

    /**
     Checks the consent state for a feature.

     - parameter feature: The feature to check the consent state for.
     - returns: the current state of consent.
     */
    @objc
    public func state(ofFeature feature: Feature) -> Consent.State {
        return consent(forFeature: feature)?.state ?? .unknown
    }


    // MARK: Campaigns

    /**
     User consents to run a specific campaign.

     - parameter campaignId: The campaign ID.
     - parameter campaign: The campaign.
     - parameter length: OPTIONAL. The customized length of the measurement period, no longer than the maximum period.
     - returns: The created `CampaignConsent`
     */
    @discardableResult
    public func grant(campaign campaignId: String, _ campaign: Campaign, length: TimeInterval?) -> CampaignConsent {
        if let period = campaign.nextTotalMeasurementPeriod {
            let end: Date

            // If a length is provided, use that, but no longer than the maximum period.
            if let length = length {
                end = min(period.end, period.start.addingTimeInterval(length))
            }
            else {
                end = period.end
            }

            // Always overwrite, since this might be a refreshed consent for a new period.
            campaigns[campaignId] = Consent(granted: true, period.start, end)
        }
        else {
            // Don't overwrite original grant timestamp.
            if campaigns[campaignId] == nil || !campaigns[campaignId]!.granted {

                // Consent is technically granted, but has no effect, as start and end
                // will be set the same.
                campaigns[campaignId] = Consent(granted: true)
            }
        }

        return consent(forCampaign: campaignId)!
    }

    /**
     User consents to run a specific campaign.

     - parameter campaignId: The campaign ID.
     - parameter campaign: The campaign.
     - parameter length: OPTIONAL. The custimized length of the measurement period, no longer than the maximum period.
     - returns: The created `CampaignConsent`
     */
    @objc
    @available(*, unavailable)
    public func grant(campaign campaignId: String, _ campaign: Campaign, length: NSNumber?) -> CampaignConsent {
        return grant(campaign: campaignId, campaign, length: length?.doubleValue)
    }

    /**
     User denies consent to run a specific campaign.
     */
    @objc
    @discardableResult
    public func deny(campaign campaignId: String) -> CampaignConsent {
        // Don't overwrite original deny timestamp.
        if campaigns[campaignId] == nil || campaigns[campaignId]!.granted {
            campaigns[campaignId] = Consent(granted: false)
        }

        return consent(forCampaign: campaignId)!
    }

    /**
     Returns the consent for a given campaign, if any available.

     - parameter campaignId: The campaign ID to get the consent for.
     - returns: the `CampaignConsent` for the given camapign or `nil`, if consent unknown.
     */
    @objc
    public func consent(forCampaign campaignId: String) -> CampaignConsent? {
        guard let consent = campaigns[campaignId] else {
            return nil
        }

        return CampaignConsent(campaignId, consent)
    }

    /**
     Checks the consent state for a campaign.

     - parameter campaignId: The campaign ID to check the consent state for.
     - returns: the current state of consent.
     */
    @objc
    public func state(ofCampaign campaignId: String) -> Consent.State {
        return consent(forCampaign: campaignId)?.state ?? .unknown
    }

    // MARK: Public Methods

    @objc
    open override var description: String {
        return String(format: "[%@: features=%@, campaigns=%@]",
                      String(describing: type(of: self)), features, campaigns)
    }
}
