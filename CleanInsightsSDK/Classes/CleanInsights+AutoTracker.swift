//
//  CleanInsights+AutoTracker.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 25.05.23.
//  Copyright © 2023 Guardian Project. All rights reserved.
//

import Foundation

@objc
extension CleanInsights {

    // #MARK: Auto Tracker

    @objc
    public private(set) static var autoTrackCi: CleanInsights?

    /**
     Instantiates a singleton `CleanInsights` object with a default configuration
     and a default campaign named "visits" which never expires.

     The "visits" campaign will have an `aggregationPeriodLength` of 1 day.
     That means, starts will be collected during 24 hours and then sent to the server,
     evenly distributed over that day, in order to keep user privacy.

     After instantiation, it will automatically track a start of the app, but ONLY, if consent is given.

     You will need to show some consent form to the user, so the user
     is able to agree to this tracking before setting consent.

     Refer to the example code to see how consent is handled.

     - parameter server: The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.
     - parameter siteId: The Matomo site ID to record page visits for.
     - parameter campaigns: (Additional) campaign configurations. OPTIONAL
     - returns: The instantiated `CleanInsights` object.
     */
    @discardableResult
    @objc
    public static func autoTrack(server: URL, siteId: Int, campaigns: [String: Campaign]? = nil) -> CleanInsights
    {
        if let ci = autoTrackCi {
            return ci
        }

        var campaigns = campaigns ?? [:]

        if campaigns["visits"] == nil {
            campaigns["visits"] = Campaign(
                start: Date(timeIntervalSince1970: 0),
                end: Date.distantFuture,
                aggregationPeriodLength: 1,
                numberOfPeriods: 50000)
        }

        let conf = Configuration(
            server: server,
            siteId: siteId,
            persistEveryNTimes: 1, // Always persist, so app doesn't need to persist manually.
            campaigns: campaigns)

        let ci = CleanInsights(configuration: conf)
        autoTrackCi = ci

        ci.measure(visit: [], forCampaign: "visits")

        return ci
    }
}
