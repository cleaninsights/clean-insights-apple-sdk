//
//  Insights.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 05.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

#if os(OSX) || os(iOS)
import WebKit
#endif

open class Insights: NSObject, Codable {

    /**
     Matomo site ID.
     */
    public let idsite: Int

    /**
     Preferred user languages as an HTTP Accept header.
     */
    public let lang: String?

    /**
     User Agent string.
     */
    public let ua: String?

    /**
     `Visit` data points.
     */
    public let visits: [Visit]

    /**
     `Event` data points.
     */
    public let events: [Event]

    public var isEmpty: Bool {
        return visits.isEmpty && events.isEmpty
    }

    static var ua: String? {
        #if os(watchOS)
        return "watchOS \(currentPlatform())"

        #elseif os(tvOS)
        return "tvOS \(currentPlatform())"

        #elseif os(OSX)
        let webView = WebView(frame: .zero)
        return webView.stringByEvaluatingJavaScript(from: "navigator.userAgent") ?? ""

        #elseif os(iOS)
            return "iOS \(currentPlatform())"
        #endif
    }

    /**
     Create an `Insights` object according to configuration with all data from the store which is due for
     offloading to the server.

     - parameter conf: The current configuration.
     - parameter store: The current measurement and consents store.
     */
    public init(_ conf: Configuration, _ store: Store) {
        idsite = conf.siteId

        lang = !conf.serverSideAnonymousUsage && store.consents.state(ofFeature: .lang) == .granted
            ? Locale.httpAcceptLanguage
            : nil

        ua = !conf.serverSideAnonymousUsage && store.consents.state(ofFeature: .ua) == .granted
            ? Insights.ua
            : nil

        Insights.purge(conf, store)

        let now = Date()

        let filter = { (_ dp: DataPoint) -> Bool in
            return conf.campaigns.contains(where: { $0.key == dp.campaignId })
                // Only send, after aggregation period is over. `last` should contain that date!
                && now > dp.last
        }

        visits = store.visits.filter(filter)

        events = store.events.filter(filter)

        super.init()
    }


    // MARK: Public Methods

    /**
     Removes all visits and events from the given `Store`, which are also available in here.

     This should be called, when all `Insights` were offloaded at the server successfully.

     - parameter store: The store where the `Visit`s and `Event`s in here came from.
     */
    public func clean(store: Store) {
        for visit in visits {
            store.visits.removeAll { $0 == visit }
        }

        for event in events {
            store.events.removeAll { $0 == event }
        }
    }


    // MARK: Private Methods

    /*
     The platform name of the current device i.e. "iPhone1,1" or "iPad3,6"
     */
    private class func currentPlatform() -> String  {
        var size = 0
        sysctlbyname("hw.machine", nil, &size, nil, 0)

        var machine = [CChar](repeating: 0,  count: Int(size))
        sysctlbyname("hw.machine", &machine, &size, nil, 0)

        return String(cString: machine)
    }

    /**
     Removes `DataPoint`s, which are too old. These were never been sent, otherwise, they would have
     been removed, already.

     Remove them now, if they're over the threshold, to not accumulate too many `DataPoints` and
     therefore reduce privacy.
     */
    internal class func purge(_ conf: Configuration, _ store: Store) {
        let threshold = Date().addingTimeInterval(TimeInterval(conf.maxAgeOfOldData * 24 * 60 * 60 * -1))

        let filter = { (_ dp: DataPoint) -> Bool in
            return dp.last < threshold
        }

        store.visits.removeAll(where: filter)
        store.events.removeAll(where: filter)
    }
}
