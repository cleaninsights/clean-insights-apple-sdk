//
//  Configuration.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 25.09.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

@objc
open class Configuration: NSObject, Codable {

    public enum CodingKeys: String, CodingKey {
        case server
        case siteId
        case timeout
        case maxRetryDelay
        case maxAgeOfOldData
        case persistEveryNTimes
        case serverSideAnonymousUsage
        case debug
        case campaigns
    }


    /**
     The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.
     */
    @objc
    public let server: URL

    /**
     The Matomo site ID to record this data for.
     */
    @objc
    public let siteId: Int

    /**
     Connection timeout. OPTIONAL, defaults to 5 seconds.
     */
    @objc
    public let timeout: TimeInterval

    /**
     The SDK uses a truncated exponential backoff strategy on server failures. So the delay until it retries will
     rise exponentially, until it reaches `maxRetryDelay` seconds.

     OPTIONAL, defaults to 3600 seconds. (1 hour)
     */
    public let maxRetryDelay: TimeInterval

    /**
     The number in days of how long the SDK will try to keep sending old measurements. If the measurements
     become older than that, they will be purged.

     OPTIONAL, defaults to 100 days.
     */
    public let maxAgeOfOldData: Int

    /**
     Regulates, how often data persistence is done. OPTIONAL. Defaults to 10.

     If set to 1, every time something is tracked, *ALL* data is stored to disk. The more you track, the
     higher you should set this to avoid heavy load due to disk I/O.
     */
    @objc
    public let persistEveryNTimes: Int

    /**
     When set to `true`, assumes consent for all campaigns and none for features.
     Only use this, when you're running on the server and don't measure anything users
     might need to give consent to!
     */
    @objc
    public let serverSideAnonymousUsage: Bool


    /**
     When set, CleanInsights SDK will print some debug output to STDOUT. OPTIONAL. Defaults to false.
     */
    @objc
    public let debug: Bool

    /**
     Campaign configuration.
     */
    @objc
    public let campaigns: [String : Campaign]


    @objc
    public init(server: URL, siteId: Int, timeout: TimeInterval = 5,
                maxRetryDelay: TimeInterval = 3600, maxAgeOfOldData: Int = 100,
                persistEveryNTimes: Int = 10, serverSideAnonymousUsage: Bool = false,
                debug: Bool = false, campaigns: [String : Campaign])
    {
        self.server = server
        self.siteId = siteId
        self.timeout = timeout
        self.maxRetryDelay = maxRetryDelay
        self.maxAgeOfOldData = maxAgeOfOldData
        self.persistEveryNTimes = persistEveryNTimes
        self.debug = debug
        self.serverSideAnonymousUsage = serverSideAnonymousUsage
        self.campaigns = campaigns

        super.init()
    }


    // MARK: Codable

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        server = try container.decode(URL.self, forKey: .server)
        siteId = try container.decode(Int.self, forKey: .siteId)
        timeout = try container.decodeIfPresent(TimeInterval.self, forKey: .timeout) ?? 5
        maxRetryDelay = try container.decodeIfPresent(TimeInterval.self, forKey: .maxRetryDelay) ?? 3600
        maxAgeOfOldData = try container.decodeIfPresent(Int.self, forKey: .maxAgeOfOldData) ?? 100
        persistEveryNTimes = try container.decodeIfPresent(Int.self, forKey: .persistEveryNTimes) ?? 10
        serverSideAnonymousUsage = try container.decodeIfPresent(Bool.self, forKey: .serverSideAnonymousUsage) ?? false
        debug = try container.decodeIfPresent(Bool.self, forKey: .debug) ?? false
        campaigns = try container.decode([String : Campaign].self, forKey: .campaigns)

        super.init()
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(server, forKey: .server)
        try container.encode(siteId, forKey: .siteId)
        try container.encode(timeout, forKey: .timeout)
        try container.encode(maxRetryDelay, forKey: .maxRetryDelay)
        try container.encode(maxAgeOfOldData, forKey: .maxAgeOfOldData)
        try container.encode(persistEveryNTimes, forKey: .persistEveryNTimes)
        try container.encode(serverSideAnonymousUsage, forKey: .serverSideAnonymousUsage)
        try container.encode(debug, forKey: .debug)
        try container.encode(campaigns, forKey: .campaigns)
    }


    // MARK: Public Methods

    /**
     Checks configuration for some well-known problems, emits a debug message and returns false, if one found.

     - parameter debug: Function to handle the debug message.
     - returns: `true`, if config seems ok, `false` if known problems exist.
     */
    public func check(debug: (_ message: String) -> Void) -> Bool {
        if !server.absoluteString.starts(with: "http") {
            debug("Configuration problem: 'server' is not defined properly. It needs to be a full URL like this: 'https://example.org/cleaninsights.php'!")

            return false
        }

        if siteId < 1 {
            debug("Configuration problem: 'siteId' is not defined properly. It needs to be a positive integer value!")

            return false
        }

        if campaigns.keys.isEmpty {
            debug("Configuration problem: No campaign defined!")

            return false
        }

        return true
    }

    @objc
    open override var description: String {
        return String(format: "[%@: server=%@, siteId=%ld, timeout=%f, maxRetryDelay=%f, maxAgeOfOldData=%ld, persistEveryNTimes=%ld, serverSideAnonymousUsage=%d, debug=%d, campaigns=%@]",
                      String(describing: type(of: self)), server.absoluteString,
                      siteId, timeout, maxRetryDelay, maxAgeOfOldData,
                      persistEveryNTimes, serverSideAnonymousUsage, debug, campaigns)
    }
}
