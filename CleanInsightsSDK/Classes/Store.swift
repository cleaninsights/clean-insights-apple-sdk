//
//  Store.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 30.09.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

/**
The store holds the user's consents to the different `Feature`s and `Campaign`s, and their `Visit` and
`Event` measurements.

 If you want to implement your own persistence of the store (e.g. because you want to write it in a database
 instead of the file system) and your own implementation of the transmission to the Matomo/CIMP backend
 (e.g. because you want to tunnel the requests through a proxy or add your own encryption layer), then
 create a subclass of this class and and implement the `#init`, `#persist` and `#send` methods.

 If you only want to change either one or the other, you can use `DefaultStore` as a base and work from
 there.
 */
open class Store: NSObject, Codable {

    public var consents = Consents()

    public var visits = [Visit]()

    public var events = [Event]()

    /**
     - parameter args: Optional arguments your implementation might need for loading the store.
     - parameter debug: Optional function to output debug messages.
     */
    public init(_ args: [String: Any] = [:], _ debug: ((_ message: String) -> Void)? = nil) {
        debug?("Store created from the following arguments: \(args.description)")

        super.init()

        var args = args 
        args["debug"] = debug

        if let store = load(args) {
            consents = store.consents
            visits = store.visits
            events = store.events

            debug?("Data loaded from storage.")
        }
        else {
            debug?("Storage doesn't exist or isn't readable.")
        }
    }

    /**
     This method gets called from the constructor and will receive the same parameters, as the constructor got.

     - parameter args: Arguments your implementation might need for loading the store.
     - returns:Deserialized store data.
     */
    public func load(_ args: [String: Any]) -> Store? {
        assertionFailure("#load needs to be implemented in a subclass!")

        return nil
    }

    /**
     This method gets called, when the SDK is of the opinion, that persisting the `Store` is in order.

     This is partly controlled by the `Configuration.persistEveryNTimes` configuration option,
     and by calls to `CleanInsights#persist`.

     If possible, try to honor the `async` flag:
     If it's true, it is set so, because the SDK wants to reduce impact on user responsitivity as much as possible.
     If it's false, the SDK wants to make sure, that the call finishes before the app gets killed.

     Also make sure to use Cocoa's `DispatchQueue` features to not run into race conditions.
     See the `DefaultStore` implementation on how to do this.

     - parameter async: Indicates, if the persistence should be done asynchronously or synchronously.
        E.g. a persist call during the exit of an app should be done synchronously, otherwise the operation
        might never get finished because the OS kills the app too early.
     - parameter done: Callback, when the operation is finished, either successfully or not.
        Be aware that this might get called on different threads/queues.
     - parameter error: If no error is returned, the operation is considered successful and the internal counter will be set back again.
     */
    public func persist(_ async: Bool, _ done: @escaping (_ error: Error?) -> Void) {
        assertionFailure("#persist needs to be implemented in a subclass!")
    }

    /**
     This method gets called, when the SDK gathered enough data for a time period and is
     ready to send the data to a CIMP (CleanInsights Matomo Proxy).

     - parameter data: The serialized JSON for a POST request to a CIMP.
     - parameter server: The server URL from `Configuration.server`.
     - parameter timeout: The timeout in seconds from `Configuration.timeout`.
     - parameter done: Callback, when the operation is finished, either successfully or not.
        Be aware that this might get called on different threads/queues.
     - parameter error: If no error is returned, the data sent will be removed from the store and the store persisted.
     */
    public func send(_ data: Data, _ server: URL, _ timeout: TimeInterval, _ done: @escaping (_ error: Error?) -> Void) {
        assertionFailure("#send needs to be implemented in a subclass!")
    }
}
