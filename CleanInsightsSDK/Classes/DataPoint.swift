//
//  DataPoint.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 28.09.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

open class DataPoint: NSObject, Codable {

    public enum CodingKeys: String, CodingKey {
        case campaignId
        case times
        case first = "period_start"
        case last = "period_end"
    }

    /**
     The campaign ID this data point is for.
     */
    public let campaignId: String


    /**
     Number of times this data point has arisen between `first` and `last`. OPTIONAL, defaults to 1.
     */
    public var times: UInt

    /**
     The first time this data point has arisen. OPTIONAL, defaults to now.
     */
    public let first: Date

    /**
     The last time this data point has arisen. OPTIONAL, defaults to now.
     */
    public let last: Date


    /**
     - parameter campaignId: The campaign ID this data point is for.
     - parameter times: Number of times this data point has arisen between `first` and `last`. OPTIONAL, defaults to 1.
     - parameter first: The first time this data point has arisen. OPTIONAL, defaults to now.
     - parameter last: The last time this data point has arisen. OPTIONAL, defaults to now.
     */
    public init(campaignId: String, times: UInt = 1, first: Date = Date(), last: Date = Date()) {
        self.campaignId = campaignId
        self.times = times
        self.first = first
        self.last = last

        super.init()
    }


    // MARK: Public Methods

    open override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? DataPoint else {
            return false
        }

        return campaignId == object.campaignId
        && times == object.times
        && first == object.first
        && last == object.last
    }

    open override var description: String {
        return String(format: "[%@: campaignId=%@, times=%d, first=%@, last=%@]",
                      String(describing: type(of: self)), campaignId, times, first.description,
                      last.description)
    }
}
