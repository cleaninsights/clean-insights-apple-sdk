//
//  Campaign.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 08.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

@objcMembers
open class Campaign: NSObject, Codable {

    public enum EventAggregationRule: String, Codable {

        /**
         Just add any new value given.
         */
        case sum

        /**
         Calculate the average of the given values.
         */
        case avg
    }

    /**
     The start of the campaign. (inclusive)

     Measurements to be recorded before this date will be ignored.
     */
    public let start: Date

    /**
     The end of the campaign. (inclusive)

     Measurements to be recorded before this date will be ignored.
     */
    public let end: Date

    /**
     The length of the aggregation period in number of days. At the end of a period, the aggregated
     data will be sent to the analytics server.
     */
    public let aggregationPeriodLength: UInt

    /**
     The number of periods you want to measure in a row. Therefore, the total length in days you measure
     one user is `aggregationPeriodLength * numberOfPeriods` beginning with the first
     day of the next period after the user consented.
     */
    public let numberOfPeriods: UInt

    /**
     Will result in recording only the first time a visit or event happened per period. Useful for yes/no questions.
     */
    public let onlyRecordOnce: Bool

    /**
     The rule how to aggregate the value of an event (if any given) with subsequent calls.
     */
    public let eventAggregationRule: EventAggregationRule

    /**
     When set to true, measurements only ever start at the next full period.
     This ensures, that anonymity guaranties aren't accidentally reduced because the
     first period is very short.
     */
    @objc
    public let strengthenAnonymity: Bool

    /**
     Returns the current measurement period, aka. the period where NOW is in.

     If NOW is outside any possible period, because the campaign hasn't started, yet, or already ended,
     will return `nil`.

     The first period is defined as `aggregationPeriodLength` number of days after the `start`
     of the campaign.
     */
    public var currentMeasurementPeriod: (start: Date, end: Date)? {
        guard numberOfPeriods > 0 else {
            return nil
        }

        var now = Date()

        // Create a copy we can mess with.
        var periodEnd = start.addingTimeInterval(0)

        repeat {
            periodEnd.addTimeInterval(aggregationPeriod)
        } while periodEnd <= now

        let periodStart = max(periodEnd.addingTimeInterval(aggregationPeriod * -1), start.addingTimeInterval(0))
        periodEnd = min(periodEnd, end.addingTimeInterval(0))

        now = Date()

        guard periodStart < now && periodEnd > now else {
            return nil
        }

        return (periodStart, periodEnd)
    }

    public var nextTotalMeasurementPeriod: (start: Date, end: Date)? {
        guard let current = currentMeasurementPeriod else {
            return nil
        }

        let periodStart = strengthenAnonymity ? current.end : current.start

        // Create a copy we can mess with.
        var periodEnd = periodStart.addingTimeInterval(0)

        var counter = 0

        while counter < numberOfPeriods && periodEnd.addingTimeInterval(aggregationPeriod) <= end {
            periodEnd.addTimeInterval(aggregationPeriod)
            counter += 1
        }

        guard periodStart != periodEnd else {
            return nil
        }

        return (periodStart, periodEnd)
    }

    public lazy var aggregationPeriod: TimeInterval = {
        return TimeInterval(aggregationPeriodLength * 24 * 60 * 60)
    }()

    public init(start: Date, end: Date, aggregationPeriodLength: UInt, numberOfPeriods: UInt = 1,
                onlyRecordOnce: Bool = false, eventAggregationRule: EventAggregationRule = .sum,
                strengthenAnonymity: Bool = false) {
        self.start = start
        self.end = end
        self.aggregationPeriodLength = aggregationPeriodLength
        self.numberOfPeriods = numberOfPeriods
        self.onlyRecordOnce = onlyRecordOnce
        self.eventAggregationRule = eventAggregationRule
        self.strengthenAnonymity = strengthenAnonymity

        super.init()
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        start = try container.decode(Date.self, forKey: .start)
        end = try container.decode(Date.self, forKey: .end)
        aggregationPeriodLength = try container.decode(UInt.self, forKey: .aggregationPeriodLength)
        numberOfPeriods = try container.decodeIfPresent(UInt.self, forKey: .numberOfPeriods) ?? 1
        onlyRecordOnce = try container.decodeIfPresent(Bool.self, forKey: .onlyRecordOnce) ?? false
        eventAggregationRule = try container.decodeIfPresent(EventAggregationRule.self, forKey: .eventAggregationRule) ?? .sum
        strengthenAnonymity = try container.decodeIfPresent(Bool.self, forKey: .strengthenAnonymity) ?? false

        super.init()
    }

    open override var description: String {
        return String(format: "[%@: start=%@, end=%@, aggregationPeriodLength=%d, numberOfPeriods=%d, onlyRecordOnce=%d, eventAggregationRule=%@, strengthenAnonymity=%d]",
                      String(describing: type(of: self)), start.description,
                      end.description, aggregationPeriodLength, numberOfPeriods,
                      onlyRecordOnce, eventAggregationRule.rawValue, strengthenAnonymity)
    }

    /**
     Apply the `eventAggregationRule` to the given event with the given value.

     - parameter value: The value to apply.
     - parameter event: The event to apply the value to.
     */
    open func apply(value: Double?, to event: Event) {
        guard let value = value, !onlyRecordOnce else {
            return
        }

        let oldVal = event.value ?? 0

        switch eventAggregationRule {
            case .sum:
                event.value = oldVal + value

            case .avg:
                // times was already increased in CleanInsights#getAndMeasure!
                event.value = (oldVal * (Double(event.times) - 1) + value) / Double(event.times)
        }
    }
}
