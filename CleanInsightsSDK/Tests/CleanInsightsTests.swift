//
//  CleanInsightsTests.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 19.02.21.
//  Copyright © 2021 Guardian Project. All rights reserved.
//

import XCTest
@testable import CleanInsightsSDK

class CleanInsightsTests: XCTestCase, ConsentRequestUi {

    static let df: DateFormatter = {
        let df = DateFormatter()

        df.timeZone = .current
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"

        return df
    }()

    static let conf = Configuration(
        server: URL(string: "http://localhost:8080/ci/cleaninsights.php")!,
        siteId: 1,
        timeout: 1,
        maxRetryDelay: 1,
        maxAgeOfOldData: 1,
        persistEveryNTimes: 1,
        serverSideAnonymousUsage: false,
        debug: true,
        campaigns: ["test" : Campaign(
                        start: df.date(from: "2021-01-01T00:00:00-00:00")!,
                        end: df.date(from: "2099-12-31T23:59:59-00:00")!,
                        aggregationPeriodLength: 1,
                        numberOfPeriods: 90,
                        onlyRecordOnce: false,
                        eventAggregationRule: .avg,
                        strengthenAnonymity: false)])

    var ci: CleanInsights!

    override func setUpWithError() throws {
        guard let bundleUrl = Bundle(for: type(of: self)).url(forResource: "test", withExtension: "bundle") else {
            throw NSError(domain: String(describing: type(of: self)), code: -1,
                          userInfo: [NSLocalizedDescriptionKey: "Test app bundle is nil unexpectedly!"])
        }

        guard let url = Bundle(url: bundleUrl)?.url(forResource: "testconf", withExtension: "json") else {
            throw NSError(domain: String(describing: type(of: self)), code: -1,
                          userInfo: [NSLocalizedDescriptionKey: "testconf.json URL is nil unexpectedly!"])
        }

        let fm = FileManager.default

        if fm.fileExists(atPath: storeUrl.path) {
            try fm.removeItem(at: storeUrl)
        }

        if fm.fileExists(atPath: consentsUrl.path) {
            try fm.removeItem(at: consentsUrl)
        }

        ci = try CleanInsights(jsonConfigurationFile: url, storageDir: storeUrl.deletingLastPathComponent())
    }

    func testConf() {
        XCTAssertEqual(CleanInsightsTests.conf.description, ci.conf.description)
    }

    func testDenyConsent() {
        XCTAssertEqual(ci.state(ofFeature: .lang), .unknown)
        XCTAssertEqual(ci.state(ofFeature: .ua), .unknown)
        XCTAssertEqual(ci.state(ofCampaign: "test"), .unknown)

        ci.deny(feature: .lang)
        ci.deny(feature: .ua)
        ci.deny(campaign: "test")

        ci.storeConsents(to: consentsUrl)

        let consents = storedConsents

        XCTAssertEqual(ci.campaignConsentsCount, 1)
        XCTAssertEqual(ci.featureConsentsCount, 2)

        XCTAssertNotNil(ci.getFeatureConsent(byIndex: 1))
        XCTAssertNil(ci.getFeatureConsent(byIndex: 2))
        XCTAssertNotNil(ci.getCampaignConsent(byIndex: 0))
        XCTAssertNil(ci.getCampaignConsent(byIndex: 1))

        XCTAssertFalse(ci.isCampaignCurrentlyGranted("test"))
        XCTAssertEqual(ci.state(ofCampaign: "test"), .denied)

        XCTAssertEqual(consents?.state(ofFeature: .lang), .denied)
        XCTAssertEqual(consents?.state(ofFeature: .ua), .denied)
        XCTAssertEqual(consents?.state(ofCampaign: "test"), .denied)
    }

    func testGrantConsent() {
        XCTAssertEqual(ci.state(ofFeature: .lang), .unknown)
        XCTAssertEqual(ci.state(ofFeature: .ua), .unknown)
        XCTAssertEqual(ci.state(ofCampaign: "test"), .unknown)

        ci.grant(feature: .lang)
        ci.grant(feature: .ua)

        ci.grant(campaign: "test", length: 7 * 24 * 60 * 60)

        ci.storeConsents(to: consentsUrl)

        let consents = storedConsents

        XCTAssertEqual(ci.campaignConsentsCount, 1)
        XCTAssertEqual(ci.featureConsentsCount, 2)

        XCTAssertNotNil(ci.getFeatureConsent(byIndex: 1))
        XCTAssertNil(ci.getFeatureConsent(byIndex: 2))
        XCTAssertNotNil(ci.getCampaignConsent(byIndex: 0))
        XCTAssertNil(ci.getCampaignConsent(byIndex: 1))

        XCTAssertTrue(ci.isCampaignCurrentlyGranted("test"))
        XCTAssertEqual(ci.state(ofCampaign: "test"), .granted)

        XCTAssertEqual(consents?.state(ofFeature: .lang), .granted)
        XCTAssertEqual(consents?.state(ofFeature: .ua), .granted)
        XCTAssertEqual(consents?.state(ofCampaign: "test"), .granted)

        let consent = ci.consent(forCampaign: "test")

        let now = Date()

        let df2 = DateFormatter()
        df2.timeZone = .current
        df2.dateFormat = "yyyy-MM-dd"

        let today = Self.df.date(from: "\(df2.string(from: now))T00:00:00-00:00")

        let inaweek = today?.addingTimeInterval(7 * 24 * 60 * 60)

        XCTAssertEqual(consent?.start, today)
        XCTAssertEqual(consent?.end, inaweek)
    }

    func testGrantConsentStrengthenedAnonymity() {
        ci = strengthenedCi

        XCTAssertEqual(ci.state(ofFeature: .lang), .unknown)
        XCTAssertEqual(ci.state(ofFeature: .ua), .unknown)
        XCTAssertEqual(ci.state(ofCampaign: "test"), .unknown)

        ci.grant(feature: .lang)
        ci.grant(feature: .ua)
        ci.grant(campaign: "test")

        ci.storeConsents(to: consentsUrl)

        let consents = storedConsents

        XCTAssertEqual(ci.campaignConsentsCount, 1)
        XCTAssertEqual(ci.featureConsentsCount, 2)

        XCTAssertNotNil(ci.getFeatureConsent(byIndex: 1))
        XCTAssertNil(ci.getFeatureConsent(byIndex: 2))
        XCTAssertNotNil(ci.getCampaignConsent(byIndex: 0))
        XCTAssertNil(ci.getCampaignConsent(byIndex: 1))

        // Should only ever allowed on the start of the next measurement period,
        // which should be tomorrow, as by the tested configuration.
        XCTAssertFalse(ci.isCampaignCurrentlyGranted("test"))
        XCTAssertEqual(ci.state(ofCampaign: "test"), .notStarted)

        XCTAssertEqual(consents?.state(ofFeature: .lang), .granted)
        XCTAssertEqual(consents?.state(ofFeature: .ua), .granted)

        // Should only ever allowed on the start of the next measurement period,
        // which should be tomorrow, as by the tested configuration.
        XCTAssertEqual(consents?.state(ofCampaign: "test"), .notStarted)
    }

    func testInvalidCampaign() {
        XCTAssertNil(ci.consent(forCampaign: "foobar"))

        XCTAssertEqual(ci.state(ofCampaign: "foobar"), .unconfigured)

        ci.requestConsent(forCampaign: "foobar", self) { consent in
            XCTAssertNil(consent)
        }

        XCTAssertNil(ci.grant(campaign: "foobar"))

        XCTAssertNil(ci.deny(campaign: "foobar"))

        XCTAssertEqual(ci.campaignConsentsCount, 0)

        XCTAssertFalse(ci.isCampaignCurrentlyGranted("foobar"))

        ci.grant(campaign: "test", length: 365 * 24 * 60 * 60)

        let consent = ci.consent(forCampaign: "test")

        XCTAssertNotNil(consent)

        let campaign = ci.conf.campaigns["test"]

        XCTAssertEqual(consent!.end.timeIntervalSince1970 - consent!.start.timeIntervalSince1970, Double(campaign!.numberOfPeriods) * campaign!.aggregationPeriod)
    }

    func testPersistence() {
        ci.grant(feature: .lang)
        ci.grant(feature: .ua)
        ci.grant(campaign: "test")

        ci.measure(visit: ["foo"], forCampaign: "test")
        ci.measure(event: "foo", "bar", forCampaign: "test", name: "baz", value: 4567)

        ci.persist()

        let store = storedStore

        let today = Calendar.current.startOfDay(for: Date()).addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
        let tomorrow = Calendar.current.date(byAdding: DateComponents(day: 1), to: today)!

        XCTAssertEqual(store?.visits.description, [Visit(scenePath: ["foo"], campaignId: "test", times: 1, first: today, last: tomorrow)].description)
        XCTAssertEqual(store?.events.description, [Event(category: "foo", action: "bar", name: "baz", value: 4567, campaignId: "test", times: 1, first: today, last: tomorrow)].description)
    }

    func testPersistenceStrengthenedAnonymity() {
        ci = strengthenedCi

        ci.grant(feature: .lang)
        ci.grant(feature: .ua)
        ci.grant(campaign: "test")

        ci.measure(visit: ["foo"], forCampaign: "test")
        ci.measure(event: "foo", "bar", forCampaign: "test", name: "baz", value: 4567)

        ci.persist()
        ci.storeConsents(to: consentsUrl)

        var store = storedStore

        XCTAssertEqual(store?.consents.description, storedConsents.description)
        XCTAssertEqual(store?.visits, [Visit](), "Consent will only start tomorrow!")
        XCTAssertEqual(store?.events, [Event](), "Consent will only start tomorrow!")

        fakeYesterdayConsent()

        ci.loadConsents(from: consentsUrl)

        ci.measure(visit: ["foo"], forCampaign: "test")
        ci.measure(event: "foo", "bar", forCampaign: "test", name: "baz", value: 4567)

        ci.persist()
        ci.storeConsents(to: consentsUrl)

        store = storedStore

        let today = Calendar.current.startOfDay(for: Date()).addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
        let tomorrow = Calendar.current.date(byAdding: DateComponents(day: 1), to: today)!

        XCTAssertEqual(store?.consents.description, storedConsents.description)
        XCTAssertEqual(store?.visits.description, [Visit(scenePath: ["foo"], campaignId: "test", times: 1, first: today, last: tomorrow)].description)
        XCTAssertEqual(store?.events.description, [Event(category: "foo", action: "bar", name: "baz", value: 4567, campaignId: "test", times: 1, first: today, last: tomorrow)].description)
    }

    func testPurge() {
        let store = TestStore()

        let dayBeforeYesterday = Date().addingTimeInterval(-2 * 24 * 60 * 60)

        store.visits.append(Visit(scenePath: ["foo"], campaignId: "x", times: 1, first: dayBeforeYesterday, last: dayBeforeYesterday))
        store.events.append(Event(category: "foo", action: "bar", campaignId: "x", times: 1, first: dayBeforeYesterday, last: dayBeforeYesterday))

        Insights.purge(CleanInsightsTests.conf, store)

        XCTAssertEqual(store.visits, [Visit](), "Should have been purged.")
        XCTAssertEqual(store.events, [Event](), "Should have been purged.")

        let now = Date()

        store.visits.append(Visit(scenePath: ["foo"], campaignId: "x", times: 1, first: now, last: now))
        store.events.append(Event(category: "foo", action: "bar", campaignId: "x", times: 1, first: now, last: now))

        Insights.purge(CleanInsightsTests.conf, store)

        XCTAssertEqual(store.visits.count, 1, "Should not have been purged.")
        XCTAssertEqual(store.events.count, 1, "Should not have been purged.")
    }

    func testSerializeInsights() {
        var insights = Insights(CleanInsightsTests.conf, TestStore())

        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .secondsSince1970

        if #available(iOS 13.0, macOS 10.15, watchOS 6.0, tvOS 13.0, *) {
            encoder.outputFormatting = .withoutEscapingSlashes
        }

        XCTAssertEqual(String(data: try! encoder.encode(insights), encoding: .utf8),
                       "{\"idsite\":1,\"visits\":[],\"events\":[]}")

        let store = TestStore()
        store.visits.append(Visit(
            scenePath: ["foo", "bar"], campaignId: "test", times: 1,
            first: Self.df.date(from: "2022-01-01T00:00:00-00:00")!,
            last: Self.df.date(from: "2022-01-02T00:00:00-00:00")!))

        store.events.append(Event(
            category: "foo", action: "bar", name: "baz", value: 6.66, campaignId: "test", times: 1,
            first: Self.df.date(from: "2022-01-01T00:00:00-00:00")!,
            last: Self.df.date(from: "2022-01-02T00:00:00-00:00")!))

        let c = Configuration(server: Self.conf.server,
                              siteId: Self.conf.siteId,
                              timeout: Self.conf.timeout,
                              maxRetryDelay: Self.conf.maxRetryDelay,
                              maxAgeOfOldData: 30000,
                              persistEveryNTimes: Self.conf.persistEveryNTimes,
                              serverSideAnonymousUsage: Self.conf.serverSideAnonymousUsage,
                              debug: Self.conf.debug,
                              campaigns: Self.conf.campaigns)

        insights = Insights(c, store)

        XCTAssertEqual(String(data: try! encoder.encode(insights), encoding: .utf8), "{\"idsite\":1,\"visits\":[{\"campaignId\":\"test\",\"period_start\":1640995200,\"times\":1,\"period_end\":1641081600,\"action_name\":\"foo/bar\"}],\"events\":[{\"category\":\"foo\",\"action\":\"bar\",\"times\":1,\"period_start\":1640995200,\"campaignId\":\"test\",\"value\":6.6600000000000001,\"period_end\":1641081600,\"name\":\"baz\"}]}")
    }

    func testServerTest() {
        ci = CleanInsights(configuration: CleanInsightsTests.conf, store: TestStore())

        ci.testServer({ error in
            XCTAssertNil(error)
        })
        
    }

    func testInvalidConfigs() {
        testConfigCheck("/", -1, [:], isGood: false)

        testConfigCheck("/", -1, ["foobar": Campaign(start: Date(), end: Date(), aggregationPeriodLength: 0)], isGood: false)

        testConfigCheck("/", 1, ["foobar": Campaign(start: Date(), end: Date(), aggregationPeriodLength: 0)], isGood: false)

        testConfigCheck("https://example.org/", 1, ["foobar": Campaign(start: Date(), end: Date(), aggregationPeriodLength: 0)], isGood: true)
    }

    func testDefaultStoreSend() {
        let store = DefaultStore()

        let url = URL(string: "http://example.org/test")

        XCTAssertNotNil(url)

        store.send(Data(), url!, 1.5) { error in
            XCTAssertNotNil(error)
            XCTAssertTrue(error?.localizedDescription.starts(with: "HTTP Error 404: Not Found") ?? false)
        }
    }

    func testAutoTrack() {
        guard let storeUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            .first?.appendingPathComponent("cleaninsights.plist")
        else {
            XCTFail("Could not initialize store URL.")

            return
        }

        try? FileManager.default.removeItem(at: storeUrl)

        let store = DefaultStore()

        store.consents.campaigns["visits"] = CampaignConsent("visits", Consent(granted: true, Date(), .distantFuture))

        store.persist(false) { error in
            XCTAssertNil(error)
        }

        XCTAssertNil(CleanInsights.autoTrackCi)

        CleanInsights.autoTrack(server: Self.conf.server, siteId: Self.conf.siteId)

        let group = DispatchGroup()
        group.enter()

        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 1) {
            let store: DefaultStore

            do {
                store = try self.readStore(storeUrl)
            }
            catch {
                return XCTFail(error.localizedDescription)
            }

            var components = Calendar.current.dateComponents([.year, .month, .day], from: Date())
            components.timeZone = TimeZone(abbreviation: "UTC")

            let first = Calendar.current.date(from: components) ?? Date()
            let last = first.addingTimeInterval(24 * 60 * 60)

            XCTAssertEqual(store.visits.first, Visit(scenePath: [""], campaignId: "visits", first: first, last: last))

            do {
                try FileManager.default.removeItem(at: storeUrl)
            }
            catch {
                XCTFail(error.localizedDescription)
            }

            group.leave()
        }

        let result = group.wait(timeout: .now() + 2)

        XCTAssertEqual(result, .success)

        XCTAssertNotNil(CleanInsights.autoTrackCi)
    }

    private var consentsUrl: URL {
        if let url = FileManager.default.urls(
            for: .cachesDirectory, in: .userDomainMask).first?
            .appendingPathComponent("consents.plist")
        {
            print(url.absoluteString)
            return url
        }

        XCTFail("Could not allocate a file to store consents to!")

        return URL(fileURLWithPath: "")
    }

    private var storedConsents: Consents! {
        do {
            let data = try Data(contentsOf: consentsUrl)

            return try PropertyListDecoder().decode(Consents.self, from: data)
        }
        catch {
            XCTFail(error.localizedDescription)
        }

        return nil
    }

    private var storeUrl: URL {
        if let url = FileManager.default.urls(
            for: .cachesDirectory, in: .userDomainMask).first?
            .appendingPathComponent("cleaninsights.plist")
        {
            print(url.absoluteString)
            return url
        }

        XCTFail("Could not allocate a file to store consents to!")

        return URL(fileURLWithPath: "")
    }

    private var storedStore: DefaultStore! {
        do {
            return try readStore(storeUrl)
        }
        catch {
            XCTFail(error.localizedDescription)
        }

        return nil
    }

    func readStore(_ url: URL) throws -> DefaultStore {
        let data = try Data(contentsOf: url)

        return try PropertyListDecoder().decode(DefaultStore.self, from: data)
    }

    private var strengthenedCi: CleanInsights {
        let co = CleanInsightsTests.conf

        let key = co.campaigns.first?.key

        XCTAssertNotNil(key, "No campaign found!")

        let ca = co.campaigns.first!.value

        let fm = FileManager.default

        if fm.fileExists(atPath: storeUrl.path) {
            try? fm.removeItem(at: storeUrl)
        }

        if fm.fileExists(atPath: consentsUrl.path) {
            try? fm.removeItem(at: consentsUrl)
        }

        return CleanInsights(
            configuration: Configuration(
                server: co.server,
                siteId: co.siteId,
                timeout: co.timeout,
                maxRetryDelay: co.maxRetryDelay,
                maxAgeOfOldData: co.maxAgeOfOldData,
                persistEveryNTimes: co.persistEveryNTimes,
                serverSideAnonymousUsage: co.serverSideAnonymousUsage,
                debug: co.debug,
                campaigns: [key! : Campaign(
                    start: ca.start,
                    end: ca.end,
                    aggregationPeriodLength: ca.aggregationPeriodLength,
                    numberOfPeriods: ca.numberOfPeriods,
                    onlyRecordOnce: ca.onlyRecordOnce,
                    eventAggregationRule: ca.eventAggregationRule,
                    strengthenAnonymity: true)]),
            storageDir: storeUrl.deletingLastPathComponent())
    }

    private func fakeYesterdayConsent() {
        let consents = storedConsents

        var minusADay = DateComponents()
        minusADay.day = -1

        guard let yesterday = Calendar.current.date(byAdding: minusADay, to: Date()) else {
            return
        }

        var threeDays = DateComponents()
        threeDays.day = 3

        guard let end = Calendar.current.date(byAdding: threeDays, to: yesterday) else {
            return
        }

        consents?.campaigns["test"] = Consent(granted: true, yesterday, end)

        do {
            let data = try PropertyListEncoder().encode(consents)

            try data.write(to: consentsUrl)
        }
        catch {
            XCTFail(error.localizedDescription)
        }
    }

    private func testConfigCheck(_ server: String, _ siteId: Int, _ campaigns: [String : Campaign], isGood: Bool) {
        let conf = Configuration(server: URL(string: server)!, siteId: siteId, campaigns: campaigns)

        var count = 0

        let result = conf.check { _ in
            count += 1
        }

        if isGood {
            XCTAssertEqual(count, 0)
            XCTAssertTrue(result)
        }
        else {
            XCTAssertEqual(count, 1)
            XCTAssertFalse(result)
        }
    }

    private class TestStore: Store {

        override func load(_ args: [String : Any]) -> Store? {
            return nil
        }

        override func send(_ data: Data, _ server: URL, _ timeout: TimeInterval, _ done: @escaping (Error?) -> Void) {
            DispatchQueue.main.async {
                done(URLError(.badServerResponse, userInfo: [NSLocalizedDescriptionKey: "HTTP Error 400: "]))
            }
        }
    }

    // MARK: ConsentRequestUi

    func show(feature: Feature, _ complete: @escaping CompleteFeature) {
        // We should not end up here.
        XCTAssertTrue(false)
    }

    func show(campaignId: String, campaign: Campaign, _ complete: @escaping CompleteCampaign) {
        // We should not end up here.
        XCTAssertTrue(false)
    }
}
