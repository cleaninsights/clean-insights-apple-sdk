#
# Be sure to run `pod lib lint CleanInsightsSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CleanInsightsSDK'
  s.version          = '2.7.0'
  s.summary          = 'A privacy-preserving measurement framework.'

  s.description      = <<-DESC
  Clean Insights gives developers a way to plug into a secure,
  private measurement platform.
  It is focused on assisting in answering key questions about app usage patterns,
  and not on enabling invasive surveillance of all user habits.
  Our approach provides programmatic levers to pull to cater to specific use
  cases and privacy needs.
  It also provides methods for user interactions that are ultimately empowering
  instead of alienating.
  DESC

  s.homepage         = 'https://cleaninsights.org'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Benjamin Erhart' => 'berhart@netzarchitekten.com' }
  s.source           = { :git => 'https://gitlab.com/cleaninsights/clean-insights-apple-sdk.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/guardianproject'

  s.ios.deployment_target = '9.3'
  s.osx.deployment_target = '10.13'
  s.watchos.deployment_target = '2.0'
  s.tvos.deployment_target = '9.0'

  s.swift_versions = ['5.0']

  s.source_files = 'CleanInsightsSDK/Classes/**/*'

  s.test_spec 'Tests' do |t|
      t.ios.deployment_target = '9.3'
      t.source_files = 'CleanInsightsSDK/Tests/**/*.swift'
      t.resource_bundles = {'test' => ['CleanInsightsSDK/Tests/**/*.json']}
  end
end
