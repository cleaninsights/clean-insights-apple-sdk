//
//  AppDelegate.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 03.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Cocoa
import CleanInsightsSDK

@main
class AppDelegate: NSObject, NSApplicationDelegate {

    var start = Date()

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationDidBecomeActive(_ notification: Notification) {
        let ui = ConsentRequestUi()

        CleanInsights.shared.requestConsent(forCampaign: "test", ui) { consent in
            guard consent?.granted == true else {
                return
            }

            CleanInsights.shared.requestConsent(forFeature: .lang, ui) { _ in
                CleanInsights.shared.requestConsent(forFeature: .ua, ui)
            }

            let time = Date().timeIntervalSinceReferenceDate - self.start.timeIntervalSinceReferenceDate

            CleanInsights.shared.measure(event: "app-state", "startup-success", forCampaign: "test", name: "time-needed", value: time)
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application

        CleanInsights.shared.persist()
    }
}

extension CleanInsights {

    static let shared = try! CleanInsights(
        jsonConfigurationFile: Bundle.main.url(forResource: "cleaninsights", withExtension: "json")!)
}
