//
//  ConsentsViewController.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 03.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Cocoa
import CleanInsightsSDK

class ConsentsViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {

    private let consentColId = NSUserInterfaceItemIdentifier("consentColumn")
    private let startColId = NSUserInterfaceItemIdentifier("startColumn")
    private let endColId = NSUserInterfaceItemIdentifier("endColumn")
    private let grantedColId = NSUserInterfaceItemIdentifier("grantedColumn")
    private let consentCellId = NSUserInterfaceItemIdentifier("consentCell")
    private let startCellId = NSUserInterfaceItemIdentifier("startCell")
    private let endCellId = NSUserInterfaceItemIdentifier("endCell")
    private let grantedCellId = NSUserInterfaceItemIdentifier("grantedCell")

    @IBOutlet weak var featureTableView: NSTableView!
    @IBOutlet weak var campaignTableView: NSTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    // MARK: NSTableViewDataSource

    func numberOfRows(in tableView: NSTableView) -> Int {
        switch tableView {
        case featureTableView:
            return CleanInsights.shared.featureConsentsCount

        case campaignTableView:
            return CleanInsights.shared.campaignConsentsCount

        default:
            return 0
        }
    }


    // MARK: NSTableViewDelegate

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let view = getTableCellView(tableView, tableColumn)

        let consent = getConsent(tableView, row)

        switch tableColumn?.identifier {
        case consentColId:
            view?.textField?.stringValue = getName(of: consent)

        case startColId:
            view?.textField?.stringValue = format(date: consent?.start)

        case endColId:
            view?.textField?.stringValue = format(date: consent?.end)

        case grantedColId:
            if let checkbox = view?.viewWithTag(1) as? NSButton {
                checkbox.state = consent?.granted ?? false ? .on : .off
                checkbox.target = self
                checkbox.action = #selector(changeConsent)
            }

        default:
            break
        }

        return view
    }


    // MARK: Actions

    @objc
    func changeConsent(_ sender: NSButton) {
        var row = featureTableView.row(for: sender)

        if row > -1,
           let consent = CleanInsights.shared.getFeatureConsent(byIndex: row)
        {
            if sender.state == .on {
                CleanInsights.shared.grant(feature: consent.feature)
            }
            else {
                CleanInsights.shared.deny(feature: consent.feature)
            }

            featureTableView.reloadData(forRowIndexes: [row],
                                        columnIndexes: IndexSet(integersIn: 0...2))

            return
        }

        row = campaignTableView.row(for: sender)

        if row > -1,
           let consent = CleanInsights.shared.getCampaignConsent(byIndex: row)
        {
            if sender.state == .on {
                CleanInsights.shared.grant(campaign: consent.campaignId)
            }
            else {
                CleanInsights.shared.deny(campaign: consent.campaignId)
            }

            campaignTableView.reloadData(forRowIndexes: [row],
                                         columnIndexes: IndexSet(integersIn: 0...3))
        }
    }

    // MARK: Private Methods

    private func getTableCellView(_ tableView: NSTableView, _ tableColumn: NSTableColumn?) -> NSTableCellView? {
        var cellId: NSUserInterfaceItemIdentifier

        switch tableColumn?.identifier {
        case consentColId:
            cellId = consentCellId

        case startColId:
            cellId = startCellId

        case endColId:
            cellId = endCellId

        case grantedColId:
            cellId = grantedCellId

        default:
            return nil
        }

        return tableView.makeView(withIdentifier: cellId, owner: self) as? NSTableCellView
    }

    private func getConsent(_ tableView: NSTableView, _ row: Int) -> Consent? {
        switch tableView {
        case featureTableView:
            return CleanInsights.shared.getFeatureConsent(byIndex: row)

        case campaignTableView:
            return CleanInsights.shared.getCampaignConsent(byIndex: row)

        default:
            return nil
        }
    }

    private func getName(of consent: Consent?) -> String {
        if let consent = consent as? FeatureConsent {
            return consent.feature.description
        }
        else if let consent = consent as? CampaignConsent {
            return consent.campaignId
        }

        return ""
    }

    private func format(date: Date?) -> String {
        if let date = date {
            return ConsentRequestUi.df.string(from: date)
        }

        return ""
    }
}
