//
//  ViewController.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 03.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Cocoa
import CleanInsightsSDK

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear() {
        super.viewDidAppear()

        CleanInsights.shared.measure(visit: ["Main"], forCampaign: "test")
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

