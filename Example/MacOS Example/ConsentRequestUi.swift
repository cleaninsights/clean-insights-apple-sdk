//
//  ConsentRequestUi.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 03.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import CleanInsightsSDK

class ConsentRequestUi: CleanInsightsSDK.ConsentRequestUi {

    static var df: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeStyle = .short
        df.doesRelativeDateFormatting = true

        return df
    }()

    func show(campaignId: String, campaign: Campaign, _ complete: @escaping CompleteCampaign) {
        guard let period = campaign.nextTotalMeasurementPeriod else {
            return
        }

        let alert = NSAlert()

        alert.messageText = NSLocalizedString("Your Consent", comment: "")

        alert.informativeText = String(
            format: NSLocalizedString("We would like to run a measurement between %@ and %@ to understand some problems we noticed recently.\n\nYour help would be highly appreciated.", comment: ""),
            ConsentRequestUi.df.string(from: period.start),
            ConsentRequestUi.df.string(from: period.end))

        alert.alertStyle = .informational

        alert.addButton(withTitle: NSLocalizedString("OK", comment: ""))
        alert.addButton(withTitle: NSLocalizedString("No, sorry.", comment: ""))

        complete(alert.runModal() == .alertFirstButtonReturn, nil)
    }

    func show(feature: Feature, _ complete: @escaping CompleteFeature) {
        let alert = NSAlert()

        alert.messageText = NSLocalizedString("Your Consent", comment: "")

        alert.informativeText = String(
            format: NSLocalizedString("In case you allow us to run measurements, are you ok that we record the following?\n\n%@", comment: ""),
            feature.description)

        alert.alertStyle = .informational

        alert.addButton(withTitle: NSLocalizedString("OK", comment: ""))
        alert.addButton(withTitle: NSLocalizedString("No, sorry.", comment: ""))

        complete(alert.runModal() == .alertFirstButtonReturn)
    }
}
