//
//  ViewController.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 09/25/2020.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit
import CleanInsightsSDK

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        CleanInsights.shared.measure(visit: ["Main"], forCampaign: "test")
    }
}
