//
//  ConsentCell.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 29.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit
import CleanInsightsSDK

class ConsentCell: UITableViewCell {

    class var id: String {
        return String(describing: self)
    }

    class var nib: UINib {
        return UINib(nibName: id, bundle: nil)
    }


    @IBOutlet weak var titleLb: UILabel!

    @IBOutlet weak var grantSw: UISwitch!

    @IBOutlet weak var startLb: UILabel! {
        didSet {
            startLb.text = NSLocalizedString("Start:", comment: "")
        }
    }

    @IBOutlet weak var startValueLb: UILabel!

    @IBOutlet weak var endLb: UILabel! {
        didSet {
            endLb.text = NSLocalizedString("End:", comment: "")
        }
    }

    @IBOutlet weak var endValueLb: UILabel!

    private var consent: Consent?

    @IBAction func changeConsent() {
        if var consent = consent as? FeatureConsent {
            if grantSw.isOn {
                consent = CleanInsights.shared.grant(feature: consent.feature)
            }
            else {
                consent = CleanInsights.shared.deny(feature: consent.feature)
            }

            set(consent)
        }
        else if var consent = consent as? CampaignConsent {
            if grantSw.isOn {
                if let c = CleanInsights.shared.grant(campaign: consent.campaignId) {
                    consent = c
                }
            }
            else {
                if let c = CleanInsights.shared.deny(campaign: consent.campaignId) {
                    consent = c
                }
            }

            set(consent)
        }
    }

    @discardableResult
    func set(_ consent: FeatureConsent) -> ConsentCell {
        self.consent = consent

        titleLb.text = consent.feature.description
        grantSw.isOn = consent.granted
        startLb.text = NSLocalizedString("Since:", comment: "")
        startValueLb.text = ConsentRequestUi.df.string(from: consent.start)
        endLb.isHidden = true
        endValueLb.isHidden = true

        return self
    }

    @discardableResult
    func set(_ consent: CampaignConsent) -> ConsentCell {
        self.consent = consent

        titleLb.text = consent.campaignId
        grantSw.isOn = consent.granted
        startLb.text = NSLocalizedString("Start:", comment: "")
        startValueLb.text = ConsentRequestUi.df.string(from: consent.start)
        endLb.isHidden = false
        endValueLb.text = ConsentRequestUi.df.string(from: consent.end)
        endValueLb.isHidden = false

        return self
    }
}
