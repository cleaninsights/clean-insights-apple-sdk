//
//  AppDelegate.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 09/25/2020.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit
import WatchConnectivity
import CleanInsightsSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WCSessionDelegate {

    var window: UIWindow?

    var start = Date()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        CleanInsights.shared.persist()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        let ui = ConsentRequestUi()

        CleanInsights.shared.requestConsent(forCampaign: "test", ui) { consent in
            let time = Date().timeIntervalSinceReferenceDate - self.start.timeIntervalSinceReferenceDate

            CleanInsights.shared.measure(event: "app-state", "startup-success", forCampaign: "test", name: "time-needed", value: time)

            guard consent?.granted == true else {
                return
            }

            CleanInsights.shared.requestConsent(forFeature: .lang, ui) { _ in
                CleanInsights.shared.requestConsent(forFeature: .ua, ui) { _ in

                    // The consents file ist stored in an app group container, which
                    // works for different app extensions and watchOS 1, but...
                    if let consentsFile = Config.consentsFile {
                        CleanInsights.shared.storeConsents(to: consentsFile)

                        // ...not for watchOS 2 and up, because these extensions
                        // are now executed on the watch. So we need to transfer
                        // the file to it!
                        self.sendConsentsToWatch()
                    }
                }
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    // MARK: WCSessionDelegate

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        sendConsentsToWatch()
    }

    func sessionDidBecomeInactive(_ session: WCSession) {
        // Nothing to do here.
    }

    func sessionDidDeactivate(_ session: WCSession) {
        WCSession.default.activate()
    }

    func sendConsentsToWatch() {
        let session = WCSession.default

        if let consentsFile = Config.consentsFile,
           FileManager.default.isReadableFile(atPath: consentsFile.path)
            && session.activationState == .activated
            && session.isPaired
            && session.isWatchAppInstalled,
           let data = try? Data(contentsOf: consentsFile)
        {
            print("Send CleanInsights consents to watch.")

            session.sendMessageData(data) { reply in
                print(reply)
            } errorHandler: { error in
                print(error)
            }
        }
    }
}

extension CleanInsights {

    static let shared = try! CleanInsights(
        jsonConfigurationFile: Bundle.main.url(forResource: "cleaninsights", withExtension: "json")!)
}

