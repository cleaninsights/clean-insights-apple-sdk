//
//  ConsentCell.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 04.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit
import CleanInsightsSDK

class ConsentCell: UITableViewCell {

    class var id: String {
        return String(describing: self)
    }

    class var nib: UINib {
        return UINib(nibName: id, bundle: nil)
    }

    @IBOutlet weak var titleLb: UILabel!

    @IBOutlet weak var startValueLb: UILabel!
    
    @IBOutlet weak var endLb: UILabel!
    
    @IBOutlet weak var endValueLb: UILabel!

    @IBOutlet weak var grantSc: UISegmentedControl!
    
    private var consent: Consent?

    @IBAction func changeConsent() {
        if var consent = consent as? FeatureConsent {
            if grantSc.selectedSegmentIndex == 0 {
                consent = CleanInsights.shared.deny(feature: consent.feature)
            }
            else {
                consent = CleanInsights.shared.grant(feature: consent.feature)
            }

            set(consent)
        }
        else if var consent = consent as? CampaignConsent {
            if grantSc.selectedSegmentIndex == 0 {
                if let c = CleanInsights.shared.deny(campaign: consent.campaignId) {
                    consent = c
                }
            }
            else {
                if let c = CleanInsights.shared.grant(campaign: consent.campaignId) {
                    consent = c
                }
            }

            set(consent)
        }
    }

    @discardableResult
    func set(_ consent: FeatureConsent) -> ConsentCell {
        self.consent = consent

        titleLb.text = consent.feature.description
        grantSc.selectedSegmentIndex = consent.granted ? 1 : 0
        startValueLb.text = ConsentRequestUi.df.string(from: consent.start)
        endLb.isHidden = true
        endValueLb.isHidden = true

        return self
    }

    @discardableResult
    func set(_ consent: CampaignConsent) -> ConsentCell {
        self.consent = consent

        titleLb.text = consent.campaignId
        grantSc.selectedSegmentIndex = consent.granted ? 1 : 0
        startValueLb.text = ConsentRequestUi.df.string(from: consent.start)
        endLb.isHidden = false
        endValueLb.text = ConsentRequestUi.df.string(from: consent.end)
        endValueLb.isHidden = false

        return self
    }
}
