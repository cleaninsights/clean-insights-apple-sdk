//
//  ConsentsViewController.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 29.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit
import CleanInsightsSDK

class ConsentsViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(ConsentCell.nib, forCellReuseIdentifier: ConsentCell.id)
    }

    // MARK: UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return CleanInsights.shared.featureConsentsCount
        }

        return CleanInsights.shared.campaignConsentsCount
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: ConsentCell.id, for: indexPath) as? ConsentCell {
            if indexPath.section == 0 {
                if let consent = CleanInsights.shared.getFeatureConsent(byIndex: indexPath.row) {
                    return cell.set(consent)
                }
            }
            else {
                if let consent = CleanInsights.shared.getCampaignConsent(byIndex: indexPath.row) {
                    return cell.set(consent)
                }
            }
        }

        return UITableViewCell()
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("General Infos", comment: "")
        }

        return NSLocalizedString("Campaigns", comment: "")
    }
}
