//
//  Config.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 04.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import Foundation

extension Config {

    class var appGroup: String {
        return __appGroup as String
    }

    /**
     For the current example using watchOS 2, this is actually unneeded, since watch apps cannot use
     app group containers any more, because they now run on the watch itself.

     However, you can use app group containers to share consents with other app extensions like
     share extensions and the like.
     */
    class var consentsFile: URL? {
        return FileManager.default
            .containerURL(forSecurityApplicationGroupIdentifier: appGroup)?
            .appendingPathComponent("consents.plist")
    }
}
