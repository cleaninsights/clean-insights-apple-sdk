//
//  UIViewController+Helpers.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 12.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import UIKit

extension UIViewController {

    var top: UIViewController {
        if let vc = subViewController {
            return vc.top
        }

        return self
    }

    var subViewController: UIViewController? {
        if let vc = self as? UINavigationController {
            return vc.topViewController
        }

        if let vc = self as? UISplitViewController {
            return vc.viewControllers.last
        }

        if let vc = self as? UITabBarController {
            return vc.selectedViewController
        }

        if let vc = presentedViewController {
            return vc
        }

        return nil
    }
}
