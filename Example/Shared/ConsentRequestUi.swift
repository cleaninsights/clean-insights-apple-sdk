//
//  ConsentRequestUi.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 20.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import CleanInsightsSDK

class ConsentRequestUi: CleanInsightsSDK.ConsentRequestUi {

    static var df: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeStyle = .short
        df.doesRelativeDateFormatting = true

        return df
    }()

    private var topViewController: UIViewController? {
        return UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController?.top
    }

    func show(campaignId: String, campaign: Campaign, _ complete: @escaping CompleteCampaign) {
        guard let period = campaign.nextTotalMeasurementPeriod else {
            return
        }

        let alert = UIAlertController(
            title: NSLocalizedString("Your Consent", comment: ""),
            message: String(
                format: NSLocalizedString("We would like to run a measurement between %@ and %@ to understand some problems we noticed recently.\n\nYour help would be highly appreciated.", comment: ""),
                ConsentRequestUi.df.string(from: period.start),
                ConsentRequestUi.df.string(from: period.end)),
            preferredStyle: .alert)

        let cancel = UIAlertAction(
            title: NSLocalizedString("No, sorry.", comment: ""),
            style: .cancel
        ) { _ in
            complete(false, nil)
        }

        let ok = UIAlertAction(
            title: NSLocalizedString("OK", comment: ""),
            style: .default
        ) { _ in
            complete(true, nil)
        }

        alert.addAction(cancel)
        alert.addAction(ok)

        topViewController?.present(alert, animated: true)
    }

    func show(feature: Feature, _ complete: @escaping CompleteFeature) {
        let alert = UIAlertController(
            title: NSLocalizedString("Your Consent", comment: ""),
            message: String(
                format: NSLocalizedString("In case you allow us to run measurements, are you ok that we record the following?\n\n%@", comment: ""),
                feature.description),
            preferredStyle: .alert)

        let cancel = UIAlertAction(
            title: NSLocalizedString("No, sorry.", comment: ""),
            style: .cancel
        ) { _ in
            complete(false)
        }

        let ok = UIAlertAction(
            title: NSLocalizedString("OK", comment: ""),
            style: .default
        ) { _ in
            complete(true)
        }

        alert.addAction(cancel)
        alert.addAction(ok)

        topViewController?.present(alert, animated: true)
    }
}
