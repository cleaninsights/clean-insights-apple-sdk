//
//  Feature+description.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 20.10.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import CleanInsightsSDK

extension Feature: CustomStringConvertible {
    
    public var description: String {
        switch self {
        case .lang:
            return NSLocalizedString("Your locale", comment: "")

        case .ua:
            return NSLocalizedString("Your device type", comment: "")
        }
    }
}
