//
//  InterfaceController.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 04.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import WatchKit
import CleanInsightsSDK

class InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user

        CleanInsights.shared.measure(visit: ["Watch"], forCampaign: "test")
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
}
