//
//  ExtensionDelegate.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 04.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import WatchKit
import WatchConnectivity
import CleanInsightsSDK

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {

    func applicationDidFinishLaunching() {
        // Perform any final initialization of your application.

        WCSession.default.delegate = self
        WCSession.default.activate()
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.

        CleanInsights.shared.persist()
    }


    // MARK: WCSessionDelegate

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        // Nothing to do here.
    }

    func session(_ session: WCSession, didReceiveMessageData messageData: Data, replyHandler: @escaping (Data) -> Void) {
        print("Received CleanInsights consents.")

        if let consentsFile = Config.consentsFile {
            do {
                try messageData.write(to: consentsFile)

                CleanInsights.shared.loadConsents(from: consentsFile)
            }
            catch {
                print(error)
            }
        }
    }
}

extension CleanInsights {

    static let shared: CleanInsights = {
        try! CleanInsights(
            jsonConfigurationFile: Bundle.main.url(forResource: "cleaninsights", withExtension: "json")!,
            storageDir: nil,
            consentsFile: Config.consentsFile)
    }()
}
