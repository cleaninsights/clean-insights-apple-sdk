//
//  NotificationController.swift
//  CleanInsightsSDK
//
//  Created by Benjamin Erhart on 04.12.20.
//  Copyright © 2020 Guardian Project. All rights reserved.
//

import WatchKit
import Foundation
import UserNotifications

class NotificationController: WKUserNotificationInterfaceController {

    override init() {
        // Initialize variables here.
        super.init()
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
}
