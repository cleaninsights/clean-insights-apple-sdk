# Clean Insights SDK

## 2.7.0

- Fixed minor issues which might have broken things in edge cases.
- Allow to set reduced length of campaign consents. 
- Added `CleanInsights#autoTrack` initializer with reduced configuration
  options and automatic tracking of app starts.

## 2.6.0

- Added `consent(forFeature:)`, `consent(forCampaign:)` and `state(ofFeature:)`, `state(ofCampaign:)` 
  methods to get detailed consent information.

## 2.5.0

- Relaxed anonymity guarantees somewhat by allowing to start measuring right away by default, 
  not just at the next period. Added "strengthenAnonymity" toggle to switch back to old behaviour.
- Updated dependencies.

## 2.4.4

- Added `Configuration#check` to check for most common configuration mistakes.

## 2.4.2

- Added `testServer` helper method to `CleanInsights` object, to test for server issues 
  without needing to go through a full measurement cycle.

## 2.4.1

- Updated for Xcode 12.5
- Fixed logic of `measure:visit` and `measure:event` to make sure, `persistAndSend` gets called.
- Offer a serverSideAnonymousUsage default (false).
- Various minor improvements.

## 2.4.0

- Added truncated exponential backoff retries on server failures.
- Added automatic purge of too old unsent data.

## 2.3.0

- Fixed crash when using out of bounds index on `CleanInsights.getFeatureConsent(byIndex:)` 
  and `CleanInsights.getCampaignConsent(byIndex:)`.
-  Renamed `CleanInsights.featureCount` to `featureConsentsCount` and
  `CleanInsights.campaignCounts` to `campaignConsentsCount` for clarification.
- Fixed bug regarding user-agent evaluation on iOS.
- Slightly improved docs.
- Added some unit tests.

## 2.2.0

- Fixed issue where last data would never get sent.
- Updated to latest Xcode.
- Provide `Store` base class, so users are able to override storage and data upload.
- Added `serverSideAnonymousUsage` configuration option.

## 2.1.0

- Added watchOS and tvOS support.

## 2.0.0

- Initial release. 
- Started with 2.0.0 to be equal on Android and Apple platforms.
