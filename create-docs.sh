#!/usr/bin/env sh
cp -a Example/Shared/Config.xcconfig ~/Desktop/
git update-index --no-skip-worktree Example/Shared/Config.xcconfig
git restore Example/Shared/Config.xcconfig
echo "Your old Config.xcconfig was saved to ~/Desktop/!"
jazzy 
mv public $TMPDIR
git checkout gl-pages
rm -rf public
mv $TMPDIR/public .
